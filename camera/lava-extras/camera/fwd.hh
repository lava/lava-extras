#pragma once
#include <memory>

#define LAVA_FORWARD_DECLARE_CLASS(T)                                          \
    class T;                                                                   \
    using Shared##T = std::shared_ptr<T>;                                      \
    using Weak##T = std::weak_ptr<T>

namespace lava {
namespace camera {

LAVA_FORWARD_DECLARE_CLASS(CameraBase);
LAVA_FORWARD_DECLARE_CLASS(FixedCamera);
LAVA_FORWARD_DECLARE_CLASS(GenericCamera);
}
}

#undef LAVA_FORWARD_DECLARE_CLASS
