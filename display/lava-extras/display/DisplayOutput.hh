#pragma once
#include "fwd.hh"
#include <lava/features/IFeature.hh>

namespace lava {

namespace display {

class DisplayWindow;
class DisplayOutput : public lava::features::IFeature {
  public:
    static std::shared_ptr<DisplayOutput> create() {
        return std::make_shared<DisplayOutput>();
    }

    DisplayOutput();

    std::shared_ptr<DisplayWindow> openWindow(SharedDevice const &device,
                                              uint32_t index);

    /* IFeature overrides */
    std::vector<const char *> instanceExtensions(std::vector<const char *> const& available) override;
    std::vector<const char *> deviceExtensions() override;
};
} // namespace display
} // namespace lava
