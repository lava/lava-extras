#include "DisplayOutput.hh"
#include "DisplayWindow.hh"
#include <lava/common/str_utils.hh>

namespace lava {
namespace display {

DisplayOutput::DisplayOutput() {}

std::shared_ptr<DisplayWindow>
DisplayOutput::openWindow(const SharedDevice &device, uint32_t index) {
    return std::make_shared<DisplayWindow>(device, index);
}

std::vector<const char *>
DisplayOutput::instanceExtensions(const std::vector<const char *> &available) {
    std::vector<const char *> result = {
        VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_DISPLAY_EXTENSION_NAME,
        VK_EXT_DIRECT_MODE_DISPLAY_EXTENSION_NAME};

    bool has_direct_mode = util::contains(available, VK_EXT_DIRECT_MODE_DISPLAY_EXTENSION_NAME);
    bool has_acquire = util::contains(available, "VK_EXT_acquire_xlib_display");

    if (has_direct_mode && has_acquire) {
        result.push_back(VK_EXT_DIRECT_MODE_DISPLAY_EXTENSION_NAME);
        result.push_back("VK_EXT_acquire_xlib_display");
    }

    return result;
}

std::vector<const char *> DisplayOutput::deviceExtensions() {
    return {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
}

} // namespace display
} // namespace lava
