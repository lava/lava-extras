#include "DisplayWindow.hh"
#include <lava/common/log.hh>
#include <lava/createinfos/Images.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/Instance.hh>

namespace lava {
namespace display {

#ifdef LAVA_DISPLAY_X11_AVAILABLE
#include <X11/Xlib.h>
void acquireDisplay(vk::Instance instance, vk::PhysicalDevice phy,
                    vk::DisplayKHR disp) {
    typedef VkResult AcquireDisplayFunc(VkPhysicalDevice, Display *,
                                        VkDisplayKHR);
    auto func =
        (AcquireDisplayFunc *)instance.getProcAddr("vkAcquireXlibDisplayEXT");
    auto *xd = XOpenDisplay(nullptr);
    if (xd) {
        // Running inside an X server
        auto res = func(phy, XOpenDisplay(nullptr), disp);
        if (res != VK_SUCCESS) {
            lava::error()
                << "Could not acquire display from XServer, is it in use?";
        }
    }
}
#else
void acquireDisplay(vk::Instance, vk::PhysicalDevice, vk::DisplayKHR) {}
#endif

void DisplayWindow::buildSwapchainWith(
    const DisplayWindow::SwapchainBuildHandler &handler) {
    mSwapchainHandler = handler;
    buildSwapchain();
}

void DisplayWindow::buildSwapchain() {
    auto caps = mDevice->physical().getSurfaceCapabilitiesKHR(mSurface);
    auto pmodes = mDevice->physical().getSurfacePresentModesKHR(mSurface);

    vk::SwapchainCreateInfoKHR info;
    info.surface = mSurface;
    info.minImageCount = caps.minImageCount;
    info.imageFormat = mFormat;
    info.imageColorSpace = vk::ColorSpaceKHR::eSrgbNonlinear;
    info.imageExtent = mSurfaceInfo.imageExtent;
    info.imageArrayLayers = 1;
    info.imageUsage = vk::ImageUsageFlagBits::eColorAttachment |
                      vk::ImageUsageFlagBits::eTransferDst;
    info.preTransform = mSurfaceInfo.transform;
    info.presentMode = vk::PresentModeKHR::eImmediate;
    info.clipped = true;

    mWidth = info.imageExtent.width;
    mHeight = info.imageExtent.height;

    mChain = mDevice->handle().createSwapchainKHR(info);
    mChainViews.clear();
    mChainImages.clear();

    {
        auto chainHandles = mDevice->handle().getSwapchainImagesKHR(mChain);
        auto imgCreateInfo =
            lava::attachment2D(mWidth, mHeight, info.imageFormat);
        for (vk::Image handle : chainHandles) {
            auto image = std::make_shared<lava::Image>(
                mDevice, imgCreateInfo, handle, vk::ImageViewType::e2D);
            mChainViews.push_back(image->createView());
            mChainImages.push_back(move(image));
        }
    }
    mSwapchainHandler(mChainViews);
    mSwapchainInfo = info;
}

DisplayWindow::DisplayWindow(
    SharedDevice device, uint32_t index,
    std::vector<lava::GenericFormat> const &formatPreferences)
    : mDevice(device) {

    auto const &instance = device->instance();

    auto displays = device->physical().getDisplayPropertiesKHR();
    mDisplay = displays[index].display;

    auto planes = device->physical().getDisplayPlanePropertiesKHR();
    auto plane_idx = std::find_if(begin(planes), end(planes),
                                  [&](vk::DisplayPlanePropertiesKHR const &p) {
                                      return !p.currentDisplay ||
                                             p.currentDisplay == mDisplay;
                                  }) -
                     begin(planes);

    auto modes = device->physical().getDisplayModePropertiesKHR(mDisplay);
    mMode = modes[0].displayMode;

    auto dpcaps =
        device->physical().getDisplayPlaneCapabilitiesKHR(mMode, plane_idx);

    acquireDisplay(instance->handle(), device->physical(), mDisplay);

    mSurfaceInfo = vk::DisplaySurfaceCreateInfoKHR()
                       .setDisplayMode(mMode)
                       .setPlaneIndex(plane_idx)
                       .setPlaneStackIndex(planes[plane_idx].currentStackIndex)
                       .setImageExtent(dpcaps.maxSrcExtent);
    mSurface = instance->handle().createDisplayPlaneSurfaceKHR(mSurfaceInfo);

    auto supported = mDevice->physical().getSurfaceSupportKHR(0, mSurface);
    if (!supported)
        throw std::runtime_error("Can't present to display surface.");

    mRenderingComplete = device->handle().createSemaphore({});
    mImageReady = device->handle().createSemaphore({});

    mQueue = &device->graphicsQueue();

    mFormat = [&]() {
        auto formats = mDevice->physical().getSurfaceFormatsKHR(mSurface);
        for (auto pref : formatPreferences) {
            auto it = find_if(begin(formats), end(formats),
                              [&](vk::SurfaceFormatKHR cand) {
                                  return cand.format == pref.vkhpp();
                              });
            if (it != end(formats))
                return pref.vkhpp();
        }
        return formats[0].format;
    }();
}

DisplayWindow::~DisplayWindow() {
    mDevice->handle().destroySemaphore(mRenderingComplete);
    mDevice->handle().destroySemaphore(mImageReady);
    mDevice->instance()->handle().destroySurfaceKHR(mSurface);
}

DisplayWindow::Frame DisplayWindow::startFrame() {
    assert(mChain && "You need to provide a handler for swapchain creation.");

    while (true) {
        // TODO: count retries, risk of infinite loop!
        // TODO: consolidate into abstract window base class? -> GlfwWindow
        try {
            auto result = mDevice->handle().acquireNextImageKHR(mChain, 1e9, mImageReady, {});

            if(result.result == vk::Result::eTimeout ||
               result.result == vk::Result::eNotReady) {
                lava::error() << "GlfwWindow::startFrame(): acquireNextImage timed out (>1s)";
                continue;
            }
            if(result.result == vk::Result::eSuboptimalKHR) {
                mDevice->handle().waitIdle();
                buildSwapchain();
                continue;
            }

            mPresentIndex = result.value;
        }
        catch(vk::OutOfDateKHRError) {
            mDevice->handle().waitIdle();
            buildSwapchain();
            continue;
        }

        return {this};
    }
}

DisplayWindow::Frame::Frame(DisplayWindow::Frame &&rhs) : window(rhs.window) {
    rhs.window = nullptr;
}

DisplayWindow::Frame::~Frame() {
    if (!window)
        return;

    vk::PresentInfoKHR info;
    info.pImageIndices = &window->mPresentIndex;
    info.pSwapchains = &window->mChain;
    info.swapchainCount = 1;
    info.pWaitSemaphores = &window->mRenderingComplete;
    info.waitSemaphoreCount = 1;

    auto result = window->mQueue->handle().presentKHR(info);
    if (result == vk::Result::eErrorOutOfDateKHR) {
        std::cout << "DisplayWindow: Rebuilding swapchain" << std::endl;
        window->mDevice->handle().waitIdle();
        window->buildSwapchain();
    }
}

DisplayWindow::Frame::Frame(DisplayWindow *parent) : window(parent) {}

} // namespace display
} // namespace lava
