#include "TiledDisplay.hh"
#include "DisplayOutput.hh"
#include "DisplayWindow.hh"
#include <lava/createinfos/Images.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/MemoryChunk.hh>
#include <thread>
#include <iostream>

namespace lava {

namespace display {

lava::display::TiledDisplay::TiledDisplay(
    const std::vector<lava::SharedDevice> &devices,
    const lava::display::TiledDisplay::Config &config)
    : mConfig(config), mDevices(devices) {

    auto output = devices[0]->get<DisplayOutput>();
    if (!output)
        throw std::logic_error("TiledDisplay: all devices need to have the "
                               "DisplayOutput feature.");

    for (auto const &c : config.tiles) {
        mWindows.push_back(output->openWindow(mDevices[c.device], c.display));
        mWindows.back()->buildSwapchainWith(
            [&](std::vector<SharedImageView> const &) {});
    }

    auto ci = lava::ImageCreateInfo()
                  .setTiling(vk::ImageTiling::eLinear)
                  .setCombinedType(vk::ImageViewType::e2D)
                  .setSize(config.sourceWidth, config.sourceHeight)
                  .setFormat(config.sourceFormat)
                  .setMipLevels(1);

    for (auto const &d : devices) {
        auto make_img = [&]() {
            auto img = ci.create(d);
            img->realizeRAM();
            img->changeLayout(vk::ImageLayout::eGeneral);
            return img;
        };
        mHostImages.push_back({{make_img(), make_img()}});
    }
}

void TiledDisplay::submit(const SharedImage &image) {
    auto master_idx =
        std::find(begin(mDevices), end(mDevices), image->device()) -
        begin(mDevices);
    if (master_idx == mDevices.size())
        throw std::logic_error(
            "For now you should present to the original device, too");

    // if true, we can skip transferring to other devices
    bool all_master = std::all_of(begin(mConfig.tiles), end(mConfig.tiles),
        [&](TileConfig const& cfg) { return cfg.device == master_idx; });

    // Step 1 - GPUs-Side: Work with mHostImages[gpu_side]

    if (!all_master) {
        { // Step 1.1 transfer master image to Host
            auto cmd = mDevices[master_idx]->graphicsQueue().beginCommandBuffer();
            mHostImages[master_idx][gpu_side]->copyFrom(image, cmd);
            mHostImages[master_idx][gpu_side]->changeLayout(
                vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eGeneral,
                cmd);
        }
        { // Step 1.2 change (non-master) hostImages layout to transfer_src
            for (size_t i = 0; i < mDevices.size(); i++) {
                if (i == master_idx)
                    continue;
                mHostImages[i][gpu_side]->changeLayout(
                    vk::ImageLayout::eGeneral,
                    vk::ImageLayout::eTransferSrcOptimal);
            }
        }
    }

    // Step 1.3 blit and present host images
    for (size_t i = 0; i < mConfig.tiles.size(); i++) {
        auto const &tile = mConfig.tiles[i];
        auto const &window = mWindows[i];
        auto frame = window->startFrame();
        {
            auto cmd =
                mDevices[tile.device]->graphicsQueue().beginCommandBuffer();
            cmd.wait(frame.imageReady());
            cmd.signal(frame.renderingComplete());

            auto src = (tile.device == master_idx)
                           ? image
                           : mHostImages[tile.device][gpu_side];

            auto sub = vk::ImageSubresourceLayers()
                           .setAspectMask(vk::ImageAspectFlagBits::eColor)
                           .setLayerCount(1);
            auto blit =
                vk::ImageBlit()
                    .setSrcOffsets(
                        {{vk::Offset3D{tile.srcRect.offset.x,
                                       tile.srcRect.offset.y, 0},
                          vk::Offset3D(tile.srcRect.offset.x +
                                           tile.srcRect.extent.width,
                                       tile.srcRect.offset.y +
                                           tile.srcRect.extent.height,
                                       1)}})
                    .setDstOffsets(
                        {{vk::Offset3D{0, 0, 0},
                          vk::Offset3D(window->width(), window->height(), 1)}})

                    .setSrcSubresource(sub)
                    .setDstSubresource(sub);

            src->blitTo(frame.image(), cmd, blit);
            frame.image()->changeLayout(vk::ImageLayout::eTransferDstOptimal,
                                        vk::ImageLayout::ePresentSrcKHR, cmd);
        }
    }

    auto cpu_side = (gpu_side + 1) % 2;
    if (!all_master) {
        { // Step 1.4 change (non-master) hostImages layout back to general
            for (size_t i = 0; i < mDevices.size(); i++) {
                if (i == master_idx)
                    continue;
                mHostImages[i][gpu_side]->changeLayout(vk::ImageLayout::eGeneral);
            }
        }

        for (size_t i = 0; i < mDevices.size(); i++)
            if (int(i) != master_idx)
                mDevices[i]->graphicsQueue().catchUp(1);

        // Step 2: Exchange Data between hostImages
        {
            auto master_map =
                mHostImages[master_idx][cpu_side]->memoryChunk()->map();

            for (size_t i = 0; i < mHostImages.size(); i++) {
                if (i == master_idx)
                    continue;
                auto map = mHostImages[i][cpu_side]->memoryChunk()->map();
                memcpy(map.data(), master_map.data(),
                       mHostImages[master_idx][0]->levelBytes(0));
            }
        }
    }
    gpu_side = cpu_side;
}

} // namespace display
} // namespace lava
