#pragma once
#include <vector>

#include <lava/fwd.hh>
#include <lava/common/vulkan.hh>
#include "fwd.hh"

namespace lava {
namespace display {

class TiledDisplay {
  public:
    struct TileConfig {
        int device;
        int display;

        vk::Rect2D srcRect;
    };

    struct Config {
        int sourceHeight, sourceWidth;
        vk::Format sourceFormat;
        std::vector<TileConfig> tiles;
    };

    TiledDisplay(std::vector<SharedDevice> const& devices, Config const &config);

    void submit(SharedImage const &image);

  protected:
    Config mConfig;
    std::vector<SharedDisplayWindow> mWindows;
    std::vector<SharedDevice> mDevices;
    std::vector<char> mBuffer;
    std::vector<std::array<SharedImage,2>> mHostImages;
    uint32_t gpu_side = 0;
};

} // namespace display
} // namespace lava
