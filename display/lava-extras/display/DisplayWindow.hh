#pragma once
#include <functional>
#include <vector>

#include <lava/common/GlLikeFormats.hh>
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava {

namespace display {

class DisplayWindow {
  public:
    class Frame;
    using SwapchainBuildHandler =
        std::function<void(std::vector<SharedImageView>)>;

    void
    buildSwapchainWith(const DisplayWindow::SwapchainBuildHandler &handler);

    /// index of the image that should be drawn to next
    uint32_t nextIndex();

    /// wait for this semaphore before drawing to the current image
    vk::Semaphore imageReady() const { return mImageReady; }

    /// signal this semaphore when the image is ready to be presented
    vk::Semaphore renderingComplete() const {
        mRenderingCompleteCalled = true;
        return mRenderingComplete;
    }

    /// the Format of the images in the swapchain.
    /// Make sure the attachment format in your Framebuffers matches this.
    vk::Format format() { return mChainFormat.format; }

    uint32_t width() const { return mWidth; }
    uint32_t height() const { return mHeight; }

    /// Call this when window is resized
    void onResize(uint32_t w, uint32_t h);

    /// Don't use this directly, use openWindow in DisplayOutput instead
    DisplayWindow(SharedDevice device, uint32_t index,
                  std::vector<lava::GenericFormat> const &formatPreferences = {
                      lava::Format::SRGB8_ALPHA8, lava::Format::ALPHA8_SBGR8,
                      lava::Format::RGBA8, lava::Format::ABGR8});
    ~DisplayWindow();

    /// Call this before you start rendering to the window.
    /// Use the included index to select the right FBO to render to.
    /// Automatically presents the image when the return value goes out-of-scope
    Frame startFrame();

    class Frame {
      public:
        LAVA_RAII_CLASS(Frame);
        Frame(Frame &&rhs);
        ~Frame();

        uint32_t imageIndex() const { return window->mPresentIndex; }
        vk::Semaphore imageReady() const { return window->mImageReady; }
        vk::Semaphore renderingComplete() const {
            return window->mRenderingComplete;
        }

        SharedImage const &image() const {
            return window->mChainImages[window->mPresentIndex];
        }

      private:
        Frame(DisplayWindow *parent);
        DisplayWindow *window;
        friend class DisplayWindow;
    };

    vk::DisplayKHR display() const { return mDisplay; }
    vk::DisplayModeKHR mode() const { return mMode; }

    vk::SurfaceKHR const &surface() const { return mSurface; }
    vk::SurfaceFormatKHR const &surfaceFormat() const { return mChainFormat; }
    vk::SwapchainCreateInfoKHR const &swapchainInfo() const {
        return mSwapchainInfo;
    }
    size_t swapchainImageCount() const { return mChainImages.size(); }
    std::vector<SharedImage> const &chainImages() const { return mChainImages; }
    std::vector<SharedImageView> const &chainViews() const {
        return mChainViews;
    }
    SharedDevice const& device() const { return mDevice; }

  protected:
    void buildSwapchain();

    SharedDevice mDevice;
    vk::SurfaceFormatKHR mChainFormat;

    uint32_t mWidth;
    uint32_t mHeight;

    vk::Format mFormat;

    vk::DisplayKHR mDisplay;
    vk::DisplayModeKHR mMode;

    vk::SurfaceKHR mSurface;
    vk::SwapchainKHR mChain;

    vk::Semaphore mImageReady;
    vk::Semaphore mRenderingComplete;
    mutable bool mRenderingCompleteCalled = false;

    uint32_t mPresentIndex;
    lava::Queue *mQueue;

    std::vector<SharedImage> mChainImages;
    std::vector<SharedImageView> mChainViews;
    SwapchainBuildHandler mSwapchainHandler;

    vk::DisplaySurfaceCreateInfoKHR mSurfaceInfo;
    vk::SwapchainCreateInfoKHR mSwapchainInfo;
    friend class Frame;
};
} // namespace display
} // namespace lava
