#pragma once
#include <memory>

#define LAVA_FORWARD_DECLARE_CLASS(T) \
    class T; \
    using Shared ## T = std::shared_ptr<T>; \
    using Weak ## T = std::weak_ptr<T>; \
    using Unique ## T = std::unique_ptr<T>

namespace lava {
namespace display {
    LAVA_FORWARD_DECLARE_CLASS(DisplayOutput);
    LAVA_FORWARD_DECLARE_CLASS(DisplayWindow);
    LAVA_FORWARD_DECLARE_CLASS(TiledDisplay);
}
}

#undef LAVA_FORWARD_DECLARE_CLASS
