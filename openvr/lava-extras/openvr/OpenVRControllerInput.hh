#pragma once

#include "fwd.hh"
#include <functional>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <lava/common/NoCopy.hh>
#include <openvr.h>
#include <unordered_map>
#include <vector>
#include <stdexcept>

namespace lava {
namespace openvr {

struct NoControllerPresent : std::runtime_error {
    NoControllerPresent() : std::runtime_error("No controller present.") {}
};

enum ControllerSide { Left = 1, Right = 2, None = 4, Any = 7 };

struct TouchInfo {
    glm::vec2 point;
};

struct ReleaseInfo {
    glm::vec2 downPoint;
    glm::vec2 upPoint;
};

struct DragInfo {
    glm::vec2 downPoint;
    glm::vec2 prevPoint;
    glm::vec2 currPoint;
};

struct DownInfo {
    glm::vec2 touchPoint;
    glm::vec2 downPoint;
};

struct UpInfo {
    glm::vec2 touchPoint;
    glm::vec2 downPoint;
    glm::vec2 upPoint;
};

struct HardDragInfo {
    glm::vec2 touchPoint;
    glm::vec2 downPoint;
    glm::vec2 prevPoint;
    glm::vec2 currPoint;
};

class OpenVRAxis {
  public:
    OpenVRAxis(uint32_t index) : mIndex(index) {}

    using TouchCallback = std::function<void(TouchInfo)>;
    using ReleaseCallback = std::function<void(ReleaseInfo)>;
    using DragCallback = std::function<void(DragInfo)>;

    using DownCallback = std::function<void(DownInfo)>;
    using UpCallback = std::function<void(UpInfo)>;
    using HardDragCallback = std::function<void(HardDragInfo)>;

    void onTouch(TouchCallback const &cb);
    void onRelease(ReleaseCallback const &cb);
    void onDrag(DragCallback const &cb);

    void onDown(DownCallback const &cb);
    void onUp(UpCallback const &cb);
    void onHardDrag(HardDragCallback const &cb);

    void operator()(TouchCallback const &cb) { onTouch(cb); }
    void operator()(ReleaseCallback const &cb) { onRelease(cb); }
    void operator()(DragCallback const &cb) { onDrag(cb); }
    void operator()(DownCallback const &cb) { onDown(cb); }
    void operator()(UpCallback const &cb) { onUp(cb); }
    void operator()(HardDragCallback const &cb) { onHardDrag(cb); }

    bool touched() const { return mTouched; }
    bool down() const { return mDown; }

  protected:
    std::vector<TouchCallback> mTouchCallbacks;
    std::vector<ReleaseCallback> mReleaseCallbacks;
    std::vector<DragCallback> mDragCallbacks;

    std::vector<DownCallback> mDownCallbacks;
    std::vector<UpCallback> mUpCallbacks;
    std::vector<HardDragCallback> mHardDragCallbacks;

    glm::vec2 mTouchPoint;
    glm::vec2 mDownPoint;
    glm::vec2 mLastPoint;
    bool mTouched, mDown;

    uint32_t mIndex;

    friend class OpenVRController;
    void update(::vr::VRControllerState_t const &state);
};

class OpenVRButton {
  public:
    OpenVRButton(uint32_t index) : mIndex(index) {}

    using ButtonCallback = std::function<void()>;
    void onDown(ButtonCallback const &cb);
    void onUp(ButtonCallback const &cb);

    bool pressed() const { return mPressed; }

  protected:
    std::vector<ButtonCallback> mPressCallbacks;
    std::vector<ButtonCallback> mUnpressCallbacks;
    uint32_t mIndex;
    bool mPressed;

    friend class OpenVRController;
    void update(::vr::VRControllerState_t const &state);
};

class OpenVRControllerInput;
class OpenVRController {
  public:
    OpenVRController(uint32_t trackedDeviceIndex);

    OpenVRAxis &axis(size_t index) { return mAxes[index]; }
    OpenVRButton &button(size_t index) { return mButtons[index]; }

    /// Axis that the pad uses on the HTC Vive Controller
    OpenVRAxis &pad() { return mAxes[0]; }
    OpenVRAxis const &pad() const { return mAxes[0]; }
    /// Axis that the trigger uses on the HTC Vive Controller
    OpenVRAxis &trigger() { return mAxes[1]; }
    OpenVRAxis const &trigger() const { return mAxes[1]; }

    glm::mat4 const &pose() const { return mPose; }

    /// World-Space position of a point that feels natural for button-pushing
    glm::vec3 tip() const {
        return {mPose * glm::vec4(0, -0.074f, -0.039f, 1.f)};
    }

    uint32_t index() const { return mIndex; }

  protected:
    glm::mat4 mPose = glm::mat4{1.0f};
    std::vector<OpenVRAxis> mAxes;
    std::vector<OpenVRButton> mButtons;
    uint32_t mIndex;
    uint32_t mLastPacket;

    void update();

    friend class OpenVRControllerInput;
};

class OpenVRControllerInput {
  public:
    OpenVRControllerInput(const SharedOpenVRHmd &output);
    LAVA_NON_MOVABLE(OpenVRControllerInput);

    /// call this once per frame after you updated the tracking from the
    /// OpenVRHmd
    void update();

    OpenVRController &hmd();
    OpenVRController &left();
    OpenVRController &right();
    OpenVRController &tracker(int index = 0);
    OpenVRController &lighthouse(int index = 0);

    /// get any of the lighthouses, but try to find one, such that differently
    /// calibrated Vives will still find the same one
    OpenVRController &uniqueLighthouse();

    std::vector<OpenVRController *> unsorted();

    OpenVRController &nth_of_class(::vr::ETrackedDeviceClass klass, int n = 0);

  protected:
    SharedOpenVRHmd mOutput;
    std::vector<OpenVRController> mControllers;
};
} // namespace openvr
} // namespace lava
