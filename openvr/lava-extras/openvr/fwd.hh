#pragma once
#include <memory>

#define LAVA_FORWARD_DECLARE_CLASS(T)                                          \
    class T;                                                                   \
    using Shared##T = std::shared_ptr<T>;                                      \
    using Weak##T = std::weak_ptr<T>;                                          \
    using Unique##T = std::unique_ptr<T>

namespace lava {
namespace openvr {

LAVA_FORWARD_DECLARE_CLASS(OpenVRModelsRenderer);
LAVA_FORWARD_DECLARE_CLASS(OpenVRHmd);
class OpenVRController;
LAVA_FORWARD_DECLARE_CLASS(OpenVRControllerInput);

} // namespace openvr
} // namespace lava

#undef LAVA_FORWARD_DECLARE_CLASS
