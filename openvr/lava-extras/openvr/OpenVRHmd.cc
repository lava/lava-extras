#include "OpenVRHmd.hh"
#include "OpenVRTypes.hh"
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <lava/common/log.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/Instance.hh>

namespace lava {
namespace openvr {

template<class F>
static void with_vr(F&& f, bool close = true) {
    ::vr::HmdError err;
    std::cout << "VR_INIT" << std::endl;
    ::vr::VR_Init(&err, ::vr::VRApplication_Scene);
    if (err != ::vr::VRInitError_None) {
        printf("Unable to init VR runtime: %s",
               VR_GetVRInitErrorAsEnglishDescription(err));
    }
    f();
    if (close) {
        ::vr::VR_Shutdown();
        std::cout << "VR_SHUTDOWN" << std::endl;
    }
}

OpenVRHmd::OpenVRHmd() {
    with_vr([&]() {
        if (!::vr::VR_IsHmdPresent()) {
            printf("No HMD Detected by OpenVR\n");
        }

        if (!::vr::VRCompositor()) {
            printf("Compositor initialization failed\n");
        }

        ::vr::VRSystem()->GetRecommendedRenderTargetSize(&mWidth, &mHeight);
    });
}

OpenVRHmd::~OpenVRHmd() { ::vr::VR_Shutdown(); }

std::vector<const char *> OpenVRHmd::instanceExtensions() {

    std::vector<const char *> result;
    static char openvr[1024];
    with_vr([&]() {
        ::vr::VRCompositor()->GetVulkanInstanceExtensionsRequired(openvr, 1024);
    });
    if (char *tok = strtok(openvr, " ")) {
        do {
            result.push_back(tok);
            tok = strtok(nullptr, " ");
        } while (tok);
    }
    return result;
}

std::vector<const char *> OpenVRHmd::deviceExtensions() {
    std::vector<const char *> result;
    static char openvr[1024];
    with_vr([&]() {
        ::vr::VRCompositor()->GetVulkanDeviceExtensionsRequired(mPhy, openvr, 1024);
    });
    if (char *tok = strtok(openvr, " ")) {
        do {
            result.push_back(tok);
            tok = strtok(nullptr, " ");
        } while (tok);
    }
    return result;
}

void OpenVRHmd::onPhysicalDeviceSelected(vk::PhysicalDevice phy) {
    mPhy = phy;
}

void OpenVRHmd::onLogicalDeviceCreated(const SharedDevice &)
{
    ::vr::HmdError err;
    std::cout << "VR_INIT" << std::endl;
    ::vr::VR_Init(&err, ::vr::VRApplication_Scene);
    if (err != ::vr::VRInitError_None) {
        printf("Unable to init VR runtime: %s",
               VR_GetVRInitErrorAsEnglishDescription(err));
    }
}

void OpenVRHmd::submitEye(uint32_t index, const SharedImage &image) {
    auto device = image->device();

    ::vr::VRVulkanTextureData_t handle;
    handle.m_nImage = uint64_t(VkImage(image->handle()));
    handle.m_nFormat = VkFormat(image->format());
    handle.m_nHeight = image->height();
    handle.m_nQueueFamilyIndex = uint32_t(device->graphicsQueue().family());
    handle.m_nSampleCount = 1;
    handle.m_nWidth = image->width();
    handle.m_pDevice = device->handle();
    handle.m_pInstance = device->instance()->handle();
    handle.m_pPhysicalDevice = device->physical();
    handle.m_pQueue = device->graphicsQueue().handle();

    ::vr::Texture_t tex;
    tex.eColorSpace = ::vr::EColorSpace::ColorSpace_Gamma;
    tex.eType = ::vr::ETextureType::TextureType_Vulkan;
    tex.handle = &handle;

    auto error = ::vr::VRCompositor()->Submit(::vr::EVREye(index), &tex);
    if (error != ::vr::VRCompositorError_None) {
        lava::error() << error;
    }
}

void OpenVRHmd::submitDistortedEye(uint32_t index, const SharedImage &image) {
    auto device = image->device();

    ::vr::VRVulkanTextureData_t handle;
    handle.m_nImage = uint64_t(VkImage(image->handle()));
    handle.m_nFormat = VkFormat(image->format());
    handle.m_nHeight = image->height();
    handle.m_nQueueFamilyIndex = uint32_t(device->graphicsQueue().family());
    handle.m_nSampleCount = 1;
    handle.m_nWidth = image->width();
    handle.m_pDevice = device->handle();
    handle.m_pInstance = device->instance()->handle();
    handle.m_pPhysicalDevice = device->physical();
    handle.m_pQueue = device->graphicsQueue().handle();

    ::vr::Texture_t tex;
    tex.eColorSpace = ::vr::EColorSpace::ColorSpace_Gamma;
    tex.eType = ::vr::ETextureType::TextureType_Vulkan;
    tex.handle = &handle;

    auto error =
        ::vr::VRCompositor()->Submit(::vr::EVREye(index), &tex, nullptr,
                                   ::vr::Submit_LensDistortionAlreadyApplied);
    if (error != ::vr::VRCompositorError_None) {
        lava::error() << error;
    }
}

void OpenVRHmd::fetchTrackingData() {
    if (::vr::VRCompositor()) {
        auto err = ::vr::VRCompositor()->WaitGetPoses(mPoses.data(),
                                                    mPoses.size(), nullptr, 0);
        if (err != ::vr::VRCompositorError_None) {
            std::cout << "Tracking::update() something wrong: " << err
                      << std::endl;
        }
    }

    auto hmd_pose = mPoses[::vr::k_unTrackedDeviceIndex_Hmd];
    if (hmd_pose.bPoseIsValid) {
        mCenterModelMatrix =
            glm::transpose(vrToGlm(hmd_pose.mDeviceToAbsoluteTracking));
    }
}

glm::mat4x4 OpenVRHmd::getCenterViewMatrix() const {
    return glm::inverse(mCenterModelMatrix);
}

glm::mat4x4 OpenVRHmd::getRelativeEyeModelMatrix(uint32_t i) const {
    return glm::transpose(
        vrToGlm(::vr::VRSystem()->GetEyeToHeadTransform(::vr::EVREye(i))));
}

glm::mat4x4 OpenVRHmd::getRelativeEyeViewMatrix(uint32_t i) const {
    return glm::inverse(getRelativeEyeModelMatrix(i));
}

glm::mat4x4 OpenVRHmd::getCombinedEyeModelMatrix(uint32_t i) const {
    return mCenterModelMatrix * getRelativeEyeModelMatrix(i);
}

glm::mat4x4 OpenVRHmd::getCombinedEyeViewMatrix(uint32_t i) const {
    return glm::inverse(getCombinedEyeModelMatrix(i));
}

glm::mat4x4 OpenVRHmd::getProjectionMatrix(uint32_t i) const {
    float left, right, top, bottom;
    ::vr::VRSystem()->GetProjectionRaw(::vr::EVREye(i), &left, &right, &top,
                                     &bottom);

    float n = 0.01f; //near

    float a = 2.0 / (right - left);
    float b = 2.0 / (bottom - top);
    float c = (right + left) / (right - left);
    float d = (bottom + top) / (bottom - top);

    glm::mat4 result = {a,    0.0f, 0.0f, 0.0f,  //
                        0.0f, -b,   0.0f, 0.0f,  //
                        c,    d,    0.0f, -1.0f, //
                        0.0f, 0.0f, n, 0.0f};
    return result;
}
} // namespace openvr
} // namespace lava
