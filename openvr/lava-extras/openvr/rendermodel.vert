#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_EXT_multiview : enable
#extension GL_NVX_multiview_per_view_attributes : enable


layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aNormal;
layout(location=2) in vec2 aUV;

layout(set = 0, binding = 0) uniform CameraMatrices {
    mat4 view[2];
    mat4 proj[2];
} cams;

layout(push_constant) uniform ModelMatrix {
    mat4 mat;
} model;

layout (location = 0) out vec2 vUV;

void main() {
    vUV = aUV;

    vec3 pos = aPosition;
    gl_PositionPerViewNV[0] = cams.proj[0] * cams.view[0] * model.mat * vec4(pos, 1.0);
    gl_PositionPerViewNV[1] = cams.proj[1] * cams.view[1] * model.mat * vec4(pos, 1.0);
    gl_Position = cams.proj[gl_ViewIndex]
                * cams.view[gl_ViewIndex]
                * model.mat * vec4(pos, 1.0);
}
