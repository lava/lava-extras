#pragma once
#include "OpenVRControllerInput.hh"

#define VR_DISABLE_UNUSED_INFO_WARNING(block)                                  \
    {                                                                          \
        (void) info;                                                           \
        block                                                                  \
    }

#define VR_ON_TOUCH(block)                                                     \
    [&](lava::openvr::TouchInfo const &info)VR_DISABLE_UNUSED_INFO_WARNING(    \
        block)
#define VR_ON_RELEASE(block)                                                   \
    [&](lava::openvr::ReleaseInfo const &info)VR_DISABLE_UNUSED_INFO_WARNING(  \
        block)
#define VR_ON_DRAG(block)                                                      \
    [&](lava::openvr::DragInfo const &info)VR_DISABLE_UNUSED_INFO_WARNING(block)

#define VR_ON_DOWN(block)                                                      \
    [&](lava::openvr::DownInfo const &info)VR_DISABLE_UNUSED_INFO_WARNING(block)
#define VR_ON_UP(block)                                                        \
    [&](lava::openvr::UpInfo const &info)VR_DISABLE_UNUSED_INFO_WARNING(block)
#define VR_ON_HARD_DRAG(block)                                                 \
    [&](lava::openvr::HardDragInfo const &info)VR_DISABLE_UNUSED_INFO_WARNING( \
        block)
