#include "OpenVRControllerInput.hh"
#include "OpenVRHmd.hh"
#include "OpenVRTypes.hh"
#include <iostream>

namespace lava {

namespace openvr {

OpenVRControllerInput::OpenVRControllerInput(SharedOpenVRHmd const &output)
    : mOutput(output)

{
    auto sys = ::vr::VRSystem();
    for (auto i = 0; i < ::vr::k_unMaxTrackedDeviceCount; i++) {
        auto klass = sys->GetTrackedDeviceClass(i);
        if (klass != ::vr::ETrackedDeviceClass::TrackedDeviceClass_Invalid)
            mControllers.emplace_back(i);
    }
}

void OpenVRControllerInput::update() {

    for (auto &c : mControllers) {
        auto pose = mOutput->getPose(c.mIndex);
        if (pose.bPoseIsValid)
            c.mPose = transpose(
                vrToGlm(mOutput->getPose(c.mIndex).mDeviceToAbsoluteTracking));
        c.update();
    }
}

OpenVRController &OpenVRControllerInput::hmd() {

    return nth_of_class(::vr::ETrackedDeviceClass::TrackedDeviceClass_HMD, 0);
}

OpenVRController &OpenVRControllerInput::left() {
    auto sys = ::vr::VRSystem();

    for (auto &c : mControllers) {
        auto role = sys->GetControllerRoleForTrackedDeviceIndex(c.mIndex);
        if (role ==
            ::vr::ETrackedControllerRole::TrackedControllerRole_LeftHand) {
            return c;
        }
    }

    // Just assume the first controller is the left one if we don't have roles
    // yet.
    return nth_of_class(
        ::vr::ETrackedDeviceClass::TrackedDeviceClass_Controller, 0);
}

OpenVRController &OpenVRControllerInput::right() {
    auto sys = ::vr::VRSystem();

    for (auto &c : mControllers) {
        auto role = sys->GetControllerRoleForTrackedDeviceIndex(c.mIndex);
        if (role ==
            ::vr::ETrackedControllerRole::TrackedControllerRole_RightHand) {
            return c;
        }
    }
    // Just assume the second controller is the right one if we don't have roles
    // yet.
    return nth_of_class(
        ::vr::ETrackedDeviceClass::TrackedDeviceClass_Controller, 1);
}

OpenVRController &OpenVRControllerInput::tracker(int index) {
    return nth_of_class(
        ::vr::ETrackedDeviceClass::TrackedDeviceClass_GenericTracker, index);
}

OpenVRController &OpenVRControllerInput::lighthouse(int index) {
    return nth_of_class(
        ::vr::ETrackedDeviceClass::TrackedDeviceClass_TrackingReference, index);
}

OpenVRController &OpenVRControllerInput::uniqueLighthouse() {
    auto sys = ::vr::VRSystem();

    // for now: use the highest
    OpenVRController *highest = nullptr;
    float height = -std::numeric_limits<float>::max();
    for (auto &c : mControllers) {
        if (sys->GetTrackedDeviceClass(c.mIndex) ==
            vr::ETrackedDeviceClass::TrackedDeviceClass_TrackingReference) {
            if (c.pose()[3][1] > height) {
                height = c.pose()[3][1];
                highest = &c;
            }
        }
    }
    if (!highest) throw NoControllerPresent();
    return *highest;
}

OpenVRController &
OpenVRControllerInput::nth_of_class(vr::ETrackedDeviceClass klass, int n) {
    auto sys = ::vr::VRSystem();

    for (auto &c : mControllers) {
        auto controller_class = sys->GetTrackedDeviceClass(c.mIndex);
        if (controller_class == klass)
            if (n-- == 0)
                return c;
    }
    throw NoControllerPresent();
}

OpenVRController::OpenVRController(uint32_t trackedDeviceIndex)
    : mIndex(trackedDeviceIndex) {
    auto sys = ::vr::VRSystem();

    /// This would be the proper way, I assume:
    // vr::ETrackedPropertyError error;
    // auto butcount = sys->GetUint64TrackedDeviceProperty(
    //    mIndex, vr::ETrackedDeviceProperty::Prop_SupportedButtons_Uint64,
    //    &error);
    /// But last time I checked, it returned 15032385543 Buttons.
    auto butcount = vr::k_EButton_Max; //8ul
    for (size_t i = 0; i < butcount; i++) {
        mButtons.emplace_back(i);
    }

    for (size_t i = 0; i < 6; i++) {
        ::vr::ETrackedPropertyError error;
        auto type = sys->GetInt32TrackedDeviceProperty(
            mIndex, ::vr::ETrackedDeviceProperty(vr::Prop_Axis0Type_Int32 + i),
            &error);
        if (type != ::vr::EVRControllerAxisType::k_eControllerAxis_None) {
            mAxes.emplace_back(i);
        }
    }
}

void OpenVRController::update() {
    ::vr::VRControllerState_t state;
    ::vr::TrackedDevicePose_t pose;
    ::vr::VRSystem()->GetControllerStateWithPose(
        ::vr::ETrackingUniverseOrigin::TrackingUniverseStanding, mIndex, &state,
        sizeof(state), &pose);
    ::vr::VRSystem()->GetControllerState(mIndex, &state, sizeof(state));
    if (state.unPacketNum == mLastPacket)
        return;

    if (pose.bPoseIsValid)
        mPose = glm::transpose(vrToGlm(pose.mDeviceToAbsoluteTracking));
    for (auto &a : mAxes)
        a.update(state);
    for (auto &b : mButtons)
        b.update(state);
}

static inline glm::vec2 toGlm(::vr::VRControllerAxis_t const &ax) {
    return {ax.x, ax.y};
}

void OpenVRButton::onDown(const OpenVRButton::ButtonCallback &cb) {
    mPressCallbacks.push_back(cb);
}

void OpenVRButton::onUp(const OpenVRButton::ButtonCallback &cb) {
    mUnpressCallbacks.push_back(cb);
}

void OpenVRButton::update(const ::vr::VRControllerState_t &state) {
    auto pressed = state.ulButtonPressed & 1ull << mIndex;
    if (pressed && !mPressed) {
        for (auto &&cb : mPressCallbacks)
            cb();
    } else if (!pressed && mPressed) {
        for (auto &&cb : mUnpressCallbacks)
            cb();
    }
    mPressed = pressed;
}

void OpenVRAxis::onTouch(const OpenVRAxis::TouchCallback &cb) {
    mTouchCallbacks.push_back(cb);
}

void OpenVRAxis::onRelease(const OpenVRAxis::ReleaseCallback &cb) {
    mReleaseCallbacks.push_back(cb);
}

void OpenVRAxis::onDrag(const OpenVRAxis::DragCallback &cb) {
    mDragCallbacks.push_back(cb);
}

void OpenVRAxis::onDown(const OpenVRAxis::DownCallback &cb) {
    mDownCallbacks.push_back(cb);
}

void OpenVRAxis::onUp(const OpenVRAxis::UpCallback &cb) {
    mUpCallbacks.push_back(cb);
}

void OpenVRAxis::onHardDrag(const OpenVRAxis::HardDragCallback &cb) {
    mHardDragCallbacks.push_back(cb);
}

void OpenVRAxis::update(const ::vr::VRControllerState_t &state) {
    auto button = ::vr::EVRButtonId::k_EButton_Axis0 + mIndex;
    auto curax = toGlm(state.rAxis[mIndex]);
    auto touched = state.ulButtonTouched & 1ull << button;
    auto pressed = state.ulButtonPressed & 1ull << button;
    if (touched) {
        if (!mTouched) {
            for (auto &&cb : mTouchCallbacks)
                cb({curax});
            mTouchPoint = curax;
        } else {
            for (auto &&cb : mDragCallbacks)
                cb({mTouchPoint, mLastPoint, curax});
        }
    } else if (mTouched) {
        for (auto &&cb : mReleaseCallbacks)
            cb({mTouchPoint, curax});
    }

    if (pressed) {
        if (!mDown) {
            for (auto &&cb : mDownCallbacks)
                cb({mTouchPoint, curax});
            mDownPoint = curax;
        } else {
            for (auto &&cb : mHardDragCallbacks)
                cb({mTouchPoint, mDownPoint, mLastPoint, curax});
        }
    } else if (mDown) {
        for (auto &&cb : mUpCallbacks)
            cb({mTouchPoint, mDownPoint, curax});
    }

    mTouched = touched;
    mDown = pressed;
    mLastPoint = curax;
}
} // namespace openvr
} // namespace lava
