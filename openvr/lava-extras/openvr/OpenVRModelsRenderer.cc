#include "OpenVRModelsRenderer.hh"
#include "OpenVRTypes.hh"
#include <glm/vec3.hpp>
#include <iostream>
#include <openvr.h>
#include <thread>

#include <lava-extras/openvr/OpenVRHmd.hh>
#include <lava-extras/pack/pack.hh>
#include <lava/createinfos/Buffers.hh>
#include <lava/createinfos/DescriptorSetLayoutCreateInfo.hh>
#include <lava/createinfos/GraphicsPipelineCreateInfo.hh>
#include <lava/createinfos/PipelineLayoutCreateInfo.hh>
#include <lava/createinfos/Sampler.hh>
#include <lava/features/DebugMarkers.hh>
#include <lava/objects/Buffer.hh>
#include <lava/objects/DescriptorSet.hh>
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/ImageData.hh>
#include <lava/objects/Sampler.hh>
#include <lava/raii/ActiveRenderPass.hh>

namespace lava {
namespace openvr {

namespace {
using Vertex = ::vr::RenderModel_Vertex_t;

struct CameraData {
    std::array<glm::mat4x4, 2> view;
    std::array<glm::mat4x4, 2> proj;
};
} // namespace

OpenVRModelsRenderer::OpenVRModelsRenderer(const Subpass &forwardPass,
                                           const SharedOpenVRHmd &output)
    : mSubpass(forwardPass), mOutput(output) {
    auto const &ivr = ::vr::VRRenderModels();
    mDevice = forwardPass.pass->device();
    auto debug = mDevice->get<features::DebugMarkers>();

    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;

    auto sampler = mDevice->createSampler({});
    auto modelcount = ivr->GetRenderModelCount();
    for (size_t model_idx = 0; model_idx < modelcount; model_idx++) {
        char modelname[512] = {};
        ivr->GetRenderModelName(model_idx, modelname, 511);

        auto &model = mRenderModels[modelname];
        model.components.clear();

        ::vr::RenderModel_t *vrmodel;
        auto error = ivr->LoadRenderModel_Async(modelname, &vrmodel);
        while (error == ::vr::VRRenderModelError_Loading) {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
            error = ivr->LoadRenderModel_Async(modelname, &vrmodel);
        }

        auto compcount = ivr->GetComponentCount(modelname);
        for (size_t i = 0; i < compcount; i++) {
            char compname[512] = {};
            ivr->GetComponentName(modelname, i, compname, 511);

            char compmodelname[512] = {};
            ivr->GetComponentRenderModelName(modelname, compname, compmodelname,
                                             511);

            ::vr::RenderModel_t *vrmodel;
            auto error = ivr->LoadRenderModel_Async(compmodelname, &vrmodel);
            while (error == ::vr::VRRenderModelError_Loading) {
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
                error = ivr->LoadRenderModel_Async(compmodelname, &vrmodel);
            }

            if (error != ::vr::VRRenderModelError_None) {
                std::cerr << "Could not load model " << modelname << "/"
                          << compname << std::endl;
                continue;
            }
            if (!vrmodel)
                continue;

            {
                auto &img = mTextures[vrmodel->diffuseTextureId];
                if (!img) {
                    if (vrmodel->diffuseTextureId >= 0) {
                        ::vr::EVRRenderModelError error;
                        ::vr::RenderModel_TextureMap_t *tex;
                        while ((error = ivr->LoadTexture_Async(
                                    vrmodel->diffuseTextureId, &tex)) ==
                               ::vr::VRRenderModelError_Loading) {
                            std::this_thread::sleep_for(
                                std::chrono::milliseconds(1));
                        }

                        img = ImageData::createFromData(tex->rubTextureMapData,
                                                        tex->unWidth,
                                                        tex->unHeight)
                                  ->uploadTo(mDevice);
                        if (debug)
                            debug->mark(
                                img,
                                "OpenVRModelsRenderer - DiffuseTexture " +
                                    std::to_string(vrmodel->diffuseTextureId));
                    } else {
                        char black[4] = {};
                        img = ImageData::createFromData(&black, 1, 1)
                                  ->uploadTo(mDevice);
                        if (debug)
                            debug->mark(
                                img, "OpenVRModelsRenderer - FallbackTexture");
                    };
                    img->changeLayout(vk::ImageLayout::eTransferSrcOptimal,
                                      vk::ImageLayout::eShaderReadOnlyOptimal);
                }
            }

            auto offset = vertices.size();
            vertices.insert(vertices.end(), vrmodel->rVertexData,
                            vrmodel->rVertexData + vrmodel->unVertexCount);

            auto &comp = model.components[compname];
            comp.count = 3ul * vrmodel->unTriangleCount;
            comp.firstIndex = uint32_t(indices.size());
            comp.texture = vrmodel->diffuseTextureId;
            assert(mTextures[comp.texture]);

            std::transform(vrmodel->rIndexData,
                           vrmodel->rIndexData + 3ul * vrmodel->unTriangleCount,
                           std::back_inserter(indices),
                           [&](uint32_t index) { return offset + index; });
        }
    }

    auto device = mSubpass.pass->device();

    mModelsBuffer =
        device->createBuffer(arrayBuffer(sizeof(Vertex) * vertices.size()));
    mModelsBuffer->setDataVRAM(vertices);

    mIndexBuffer =
        device->createBuffer(indexBuffer(sizeof(uint32_t) * indices.size()));
    mIndexBuffer->setDataVRAM(indices);

    { // DS for Camera
        auto dlayoutinfo = DescriptorSetLayoutCreateInfo{};
        dlayoutinfo.addUniformBuffer(vk::ShaderStageFlagBits::eVertex);

        mCameraLayout = device->createDescriptorSetLayout(dlayoutinfo, 1);
        mCameraBuffer = device->createBuffer(uniformBuffer(sizeof(CameraData)));
        mCameraBuffer->realizeVRAM();

        mCameraDescriptor = mCameraLayout->createDescriptorSet();
        mCameraDescriptor->writeUniformBuffer(mCameraBuffer, 0);
    }

    { // DSs for Textures
        auto texlayoutinfo = DescriptorSetLayoutCreateInfo{};
        texlayoutinfo.addCombinedImageSampler(
            vk::ShaderStageFlagBits::eFragment);
        mTextureLayout = mDevice->createDescriptorSetLayout(
            texlayoutinfo, mTextures.size() + 5);

        for (auto const &tex : mTextures) {
            auto ds = mTextureLayout->createDescriptorSet();
            ds->writeCombinedImageSampler({sampler, tex.second->createView()},
                                          0);
            mTextureDescriptors[tex.first] = ds;
        }
    }

    mPipelineLayout = device->createPipelineLayout<glm::mat4>(
        {mCameraLayout, mTextureLayout});

    auto pipci = lava::GraphicsPipelineCreateInfo::defaults();
    pipci.depthStencilState.setDepthTestEnable(true).setDepthWriteEnable(true);
    pipci.addStage(pack::shader(device, "lava-extras/openvr/rendermodel.vert"));
    pipci.addStage(pack::shader(device, "lava-extras/openvr/rendermodel.frag"));
    pipci.setLayout(mPipelineLayout);
    pipci.vertexInputState.binding(0, &Vertex::vPosition, &Vertex::vNormal,
                                   &Vertex::rfTextureCoord);

    mGraphicsPipeline = mSubpass.createPipeline(pipci);
}

void OpenVRModelsRenderer::update(RecordingCommandBuffer &cmd) {

    CameraData data;
    data.view = {{mOutput->getCombinedEyeViewMatrix(0), //
                  mOutput->getCombinedEyeViewMatrix(1)}};
    data.proj = {{mOutput->getProjectionMatrix(0), //
                  mOutput->getProjectionMatrix(1)}};

    mCameraBuffer->pushData(&data, sizeof(data), cmd);
}

void OpenVRModelsRenderer::draw(InlineSubpass &cmd) {
    cmd.bindPipeline(mGraphicsPipeline);
    cmd.bindDescriptorSets({mCameraDescriptor});
    cmd.bindVertexBuffers({mModelsBuffer});
    cmd.bindIndexBuffer(mIndexBuffer, vk::IndexType::eUint32);

    auto const &sys = ::vr::VRSystem();
    auto const &ivr = ::vr::VRRenderModels();

    ::vr::RenderModel_ControllerMode_State_t modelstate = {false};

    auto &&poses = mOutput->getPoses();
    for (size_t i = 0; i < poses.size(); i++) {
        auto &&pose = poses[i];
        if (mHideMask[i])
            continue;
        if (!pose.bPoseIsValid)
            continue;

        char modelname[512] = {};
        ::vr::ETrackedPropertyError error;

        sys->GetStringTrackedDeviceProperty(
            i, ::vr::Prop_RenderModelName_String, modelname, 512, &error);

        ::vr::VRControllerState_t state;
        if (!sys->GetControllerState(i, &state, sizeof(state)))
            continue;

        glm::mat4 posematrix =
            transpose(vrToGlm(pose.mDeviceToAbsoluteTracking));

        auto const &model = mRenderModels[modelname];
        int32_t last_texture = -2;
        for (auto const &comp : model.components) {
            ::vr::RenderModel_ComponentState_t compstate;
            if (!ivr->GetComponentState(modelname, comp.first.c_str(), &state,
                                        &modelstate, &compstate))
                continue;
            auto partmatrix =
                vrToGlm(compstate.mTrackingToComponentRenderModel);
            partmatrix = transpose(partmatrix);

            if (comp.second.texture != last_texture) {
                cmd.bindDescriptorSets(
                    {mTextureDescriptors[comp.second.texture]}, 1);
                last_texture = comp.second.texture;
            }
            cmd.pushConstantBlock(posematrix * partmatrix);
            cmd.drawIndexed(comp.second.count, 1, 0, comp.second.firstIndex, 0);
        }
    }
}
} // namespace openvr
} // namespace lava
