#pragma once
#include <glm/mat4x4.hpp>
#include <lava/features/IFeature.hh>
#include <openvr.h>

namespace lava {
namespace openvr {

class OpenVRHmd : public features::IFeature {
  public:
    OpenVRHmd();
    ~OpenVRHmd();
    static std::shared_ptr<OpenVRHmd> create() {
        return std::make_shared<OpenVRHmd>();
    }

    uint32_t width() { return mWidth; }
    uint32_t height() { return mHeight; }

    std::vector<const char *> instanceExtensions() override;
    std::vector<const char *> deviceExtensions() override;
    void onPhysicalDeviceSelected(vk::PhysicalDevice phy) override;
    void onLogicalDeviceCreated(lava::SharedDevice const&) override;

    void submitEye(uint32_t index, SharedImage const &image);
    void submitDistortedEye(uint32_t index, SharedImage const &image);

    void fetchTrackingData();

    ::vr::TrackedDevicePose_t const &getPose(size_t i) const { return mPoses[i]; }
    std::array<::vr::TrackedDevicePose_t, ::vr::k_unMaxTrackedDeviceCount> const &
    getPoses() const {
        return mPoses;
    }

    glm::mat4x4 getCenterModelMatrix() const { return mCenterModelMatrix; }
    glm::mat4x4 getCenterViewMatrix() const;

    glm::mat4x4 getRelativeEyeModelMatrix(
        uint32_t i) const; // Returns the model matrix for an eye relative to
                           // the center model matrix.
    glm::mat4x4 getRelativeEyeViewMatrix(
        uint32_t i) const; // Returns the view matrix for an eye relative to the
                           // center view matrix.

    glm::mat4x4 getCombinedEyeModelMatrix(
        uint32_t i) const; // Returns the model matrix for an eye multiplied
                           // with the center model matrix.
    glm::mat4x4 getCombinedEyeViewMatrix(
        uint32_t i) const; // Returns the view matrix for an eye multiplied with
                           // the center view matrix.

    glm::mat4x4 getProjectionMatrix(uint32_t i) const;

  protected:
    uint32_t mWidth, mHeight;
    vk::PhysicalDevice mPhy;
    std::array<::vr::TrackedDevicePose_t, ::vr::k_unMaxTrackedDeviceCount> mPoses;
    glm::mat4x4 mCenterModelMatrix;

    std::vector<char const*> mInstanceExtensions;
    std::vector<char const*> mDeviceExtensions;
};

using SharedOpenVRHmd = std::shared_ptr<OpenVRHmd>;
} // namespace openvr
} // namespace lava
