#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 vUV;
layout (location = 0) out vec4 fColor;

layout (set = 1, binding = 0) uniform sampler2D uTexture;

void main() {
    //fColor = vec4(vUV, 0.0, 1.0);
    fColor = texture(uTexture, vUV);
}
