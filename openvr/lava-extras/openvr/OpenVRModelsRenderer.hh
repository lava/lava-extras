#pragma once
#include "fwd.hh"
#include <lava/fwd.hh>
#include <lava/objects/RenderPass.hh>
#include <unordered_map>
#include <glm/mat4x4.hpp>
#include <openvr.h>
#include <bitset>

namespace lava {
namespace openvr {

/**
 * @brief The OpenVRModelsRenderer class
 * A renderer for the RenderModels provided through OpenVR (controllers,
 * lighthouses)
 */
class OpenVRModelsRenderer {
  public:
    OpenVRModelsRenderer(lava::Subpass const &forwardPass,
                         SharedOpenVRHmd const &output);

    void draw(InlineSubpass &cmd);

    /// call this outside of a render pass
    void update(RecordingCommandBuffer &cmd);

    void showAll() { mHideMask.reset(); }
    void hide(uint32_t deviceIndex) { mHideMask.set(deviceIndex); }

  protected:
    SharedBuffer mModelsBuffer;
    SharedBuffer mIndexBuffer;

    struct RenderComponent {
        uint32_t firstIndex;
        uint32_t count;
        int32_t texture;
    };

    struct RenderModel {
        std::unordered_map<std::string, RenderComponent> components;
    };

    std::unordered_map<std::string, RenderModel> mRenderModels;

    RenderModel const& getModel(std::string const& name);

    SharedDescriptorSetLayout mTextureLayout;
    std::unordered_map<int32_t, SharedImage> mTextures;
    std::unordered_map<int32_t, SharedDescriptorSet> mTextureDescriptors;

    SharedDescriptorSetLayout mCameraLayout;
    SharedBuffer mCameraBuffer;
    SharedDescriptorSet mCameraDescriptor;

    SharedPipelineLayout mPipelineLayout;

    SharedGraphicsPipeline mGraphicsPipeline;
    Subpass mSubpass;

    SharedOpenVRHmd mOutput;
    SharedDevice mDevice;

    std::bitset<::vr::k_unMaxTrackedDeviceCount> mHideMask;
};
}
}
