#pragma once
#include <glm/mat4x4.hpp>
#include <openvr.h>

namespace lava {
    inline glm::mat4 vrToGlm(::vr::HmdMatrix34_t mat)
    {
        glm::mat4 result{1.0f};

        for (size_t i = 0; i < 3; i++)
        {
            for (size_t j = 0; j < 4; j++)
            {
                result[i][j] = mat.m[i][j];
            }
        }

        return result;
    }

    inline glm::mat4 vrToGlm(::vr::HmdMatrix44_t mat)
    {
        glm::mat4 result{1.0f};
        for (size_t i = 0; i < 4; i++)
        {
            for (size_t j = 0; j < 4; j++)
            {
                result[i][j] = mat.m[i][j];
            }
        }

        return result;
    }
}
