#include <lava/common/FormatInfo.hh>
#include <openvr.h>

// Vectors of floats
namespace lava {
template <>
vk::Format vkTypeOf<::vr::HmdVector2_t>::format = vk::Format::eR32G32Sfloat;
template <>
vk::Format vkTypeOf<::vr::HmdVector3_t>::format = vk::Format::eR32G32B32Sfloat;
template <>
vk::Format vkTypeOf<::vr::HmdVector4_t>::format = vk::Format::eR32G32B32A32Sfloat;
}
