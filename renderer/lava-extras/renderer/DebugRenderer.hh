#pragma once

#include <lava/objects/RenderPass.hh>
#include <lava-extras/geometry/fwd.hh>
#include <lava-extras/camera/FixedCamera.hh>

/*  A simple renderer that takes a Geometry and will interpret the first
 *  attribute as binding and the second as color to output.
 *
 *  The Geometry is drawn in the provided RenderPass, which needs to have one
 *  color attachment and one depth attachment.
 */

namespace lava {
namespace renderer {

class DebugRenderer {

  public:
    DebugRenderer(geometry::SharedGeometry const &geometry,
                  Subpass const &pass);

    void updateCameras(std::vector<camera::FixedCamera> const &cameras);
    void updateCameras(std::vector<camera::FixedCamera> const &cameras,
                       RecordingCommandBuffer &cmd);

    /// expects the vector to be of shape [view0 ... viewN, proj0 ... projN]
    void updateCameras(std::vector<glm::mat4x4> const &cameras);
    void updateCameras(std::vector<glm::mat4x4> const &cameras,
                       RecordingCommandBuffer &cmd);

    void draw(lava::InlineSubpass &cmd,
              std::vector<glm::mat4x4> const &modelMatrices);

  private:
    Subpass mRenderPass;
    geometry::SharedGeometry mGeometry;

    SharedDescriptorSetLayout mCameraLayout;
    SharedPipelineLayout mPipelineLayout;
    SharedDescriptorSet mCameraSet;

    SharedBuffer mCameraBuffer;

    SharedGraphicsPipeline mPipeline;
    SharedGraphicsPipeline mDepthPipeline;

    std::vector<glm::mat4x4> mMatricesCache;

    uint32_t mNumViews = 1;
};
}
}
