#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_EXT_multiview : enable

layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aColor;

struct Camera {
    mat4 view;
    mat4 proj;
};

layout(set = 0, binding = 0) uniform CameraMatrices {
    Camera cam[16];
} cams;

layout(push_constant) uniform PushConstants {
    mat4 model;
} pu;

layout (location = 0) out vec3 vColor;

void main() {
    gl_Position = cams.cam[gl_ViewIndex].proj
                * cams.cam[gl_ViewIndex].view
                * pu.model
                * vec4(aPosition, 1.0);
    vColor = aColor;
}
