#include "DebugRenderer.hh"
#include <lava-extras/geometry/Geometry.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Buffer.hh>
#include <lava/objects/ShaderModule.hh>
#include <lava/objects/DescriptorSet.hh>
#include <lava/createinfos/GraphicsPipelineCreateInfo.hh>
#include <lava/createinfos/DescriptorSetLayoutCreateInfo.hh>
#include <lava/createinfos/RenderPassMultiviewCreateInfoKHX.hh>
#include <lava/createinfos/Buffers.hh>
#include <lava/raii/ActiveRenderPass.hh>
#include <lava/features/MultiView.hh>
#include <glm/mat4x4.hpp>
#include <memory>
#include <bitset>
#include <lava-extras/pack/pack.hh>

namespace lava {
namespace renderer {

namespace {
struct CameraData {
    glm::mat4x4 view;
    glm::mat4x4 proj;
};
}

DebugRenderer::DebugRenderer(const geometry::SharedGeometry &geometry,
                             const Subpass &pass)
    : mGeometry(geometry), mRenderPass(pass) {

    { // Detect if the Subpass is using multiview
        auto &&info = pass.pass->createInfo();
        auto next = info.next();
        while (next) {
            if (next->type() ==
                vk::StructureType::eRenderPassMultiviewCreateInfoKHR) {
                auto rpmci =
                    std::dynamic_pointer_cast<RenderPassMultiviewCreateInfoKHX>(
                        next);
                std::bitset<32> set{rpmci->viewMasks()[pass.subpassIndex]};
                mNumViews = uint32_t(set.count());
                break;
            }
            next = next->next();
        }
    }

    auto device = pass.pass->device();

    auto dsinfo = lava::DescriptorSetLayoutCreateInfo{};
    dsinfo.addUniformBuffer(vk::ShaderStageFlagBits::eVertex);

    mCameraLayout = device->createDescriptorSetLayout(dsinfo, 1);
    mCameraSet = mCameraLayout->createDescriptorSet();

    mPipelineLayout =
        device->createPipelineLayout<glm::mat4x4>({mCameraLayout});

    mCameraBuffer = device->createBuffer(
        lava::uniformBuffer(sizeof(CameraData) * mNumViews));
    mCameraBuffer->realizeVRAM();
    mCameraBuffer->keepStagingBuffer();

    mCameraSet->writeUniformBuffer(mCameraBuffer, 0);

    auto pinfo = lava::GraphicsPipelineCreateInfo::defaults();
    pinfo.addStage(
        lava::pack::shader(device, "lava-extras/renderer/debug.frag"));
    pinfo.addStage(
        lava::pack::shader(device, "lava-extras/renderer/debug.vert"));
    //rpinfo.stage(0).specialize(0, 8ul);
    pinfo.stage(1).specialize(0, 2ul);

    pinfo.depthStencilState.setDepthTestEnable(true).setDepthWriteEnable(true);
    pinfo.setLayout(mPipelineLayout);
    pinfo.vertexInputState = mGeometry->info();

    mPipeline = pass.createPipeline(pinfo);
}

void
DebugRenderer::updateCameras(const std::vector<camera::FixedCamera> &cameras) {
    mMatricesCache.resize(mNumViews * 2);
    for (size_t i = 0; i < cameras.size(); i++) {
        mMatricesCache[i] = cameras[i].getViewMatrix();
        mMatricesCache[i + mNumViews] = cameras[i].getProjectionMatrix();
    }
    updateCameras(mMatricesCache);
}

void
DebugRenderer::updateCameras(const std::vector<camera::FixedCamera> &cameras,
                             RecordingCommandBuffer &cmd) {
    mMatricesCache.resize(mNumViews * 2);
    for (size_t i = 0; i < cameras.size(); i++) {
        mMatricesCache[i] = cameras[i].getViewMatrix();
        mMatricesCache[i + mNumViews] = cameras[i].getProjectionMatrix();
    }
    updateCameras(mMatricesCache, cmd);
}

void DebugRenderer::updateCameras(const std::vector<glm::mat4x4> &cameras) {
    mCameraBuffer->pushData(cameras);
}

void DebugRenderer::updateCameras(const std::vector<glm::mat4x4> &cameras,
                                  RecordingCommandBuffer &cmd) {
    mCameraBuffer->pushData(cameras, cmd);
}

void DebugRenderer::draw(InlineSubpass &cmd,
                         const std::vector<glm::mat4x4> &modelMatrices) {
    cmd.bindPipeline(mPipeline);
    cmd.bindDescriptorSets({mCameraSet});

    mGeometry->bind(cmd);

    for (auto&& mat : modelMatrices) {
        cmd.pushConstantBlock(mat);
        mGeometry->drawNoBind(cmd);
    }
}
}
}
