#include "glsl.hh"
#include <memory>
#include <map>
#include <mutex>
#include <extern/glslang/glslang/Public/ShaderLang.h>
#include <extern/glslang/glslang/Include/ShHandle.h>
#include <extern/glslang/SPIRV/GlslangToSpv.h>
#include <extern/glslang/SPIRV/GLSL.std.450.h>
#include <extern/glslang/SPIRV/doc.h>
#include <extern/glslang/SPIRV/disassemble.h>
#include <extern/glslang/glslang/OSDependent/osinclude.h>
#include <extern/glslang/StandAlone/ResourceLimits.h>
#include <extern/glslang/StandAlone/DirStackFileIncluder.h>
#include <lava/createinfos/GraphicsPipelineCreateInfo.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/RenderPass.hh>
#include <lava/objects/GraphicsPipeline.hh>
#include <fstream>
#include <memory>
#include "file_utils.hh"
#include <lava/common/log.hh>

namespace lava {
namespace glsl {

namespace {

struct DynamicShaderInfo {
    Input input;
    size_t moduleIndex;
    int64_t modificationTime;
};

std::map<std::weak_ptr<GraphicsPipeline>, std::vector<DynamicShaderInfo>,
         std::owner_less<std::weak_ptr<GraphicsPipeline>>> sShaderInfos;
std::mutex sShaderInfosMutex;
}

//
//   Deduce the language from the filename.  Files must end in one of the
//   following extensions:
//
//   .vert = vertex
//   .tesc = tessellation control
//   .tese = tessellation evaluation
//   .geom = geometry
//   .frag = fragment
//   .comp = compute
//
static EShLanguage findStage(const std::string &name) {
    size_t ext = name.rfind('.');
    if (ext == std::string::npos) {
        return EShLangVertex;
    }
    ++ext;
    std::string suffix = name.substr(ext, std::string::npos);

    if (suffix == "vert")
        return EShLangVertex;
    else if (suffix == "tesc")
        return EShLangTessControl;
    else if (suffix == "tese")
        return EShLangTessEvaluation;
    else if (suffix == "geom")
        return EShLangGeometry;
    else if (suffix == "frag")
        return EShLangFragment;
    else if (suffix == "comp")
        return EShLangCompute;
    return EShLangVertex;
}

static vk::ShaderStageFlagBits stageForLang(EShLanguage lang) {
    switch (lang) {
    case EShLangVertex:
        return vk::ShaderStageFlagBits::eVertex;
    case EShLangFragment:
        return vk::ShaderStageFlagBits::eFragment;
    case EShLangTessControl:
        return vk::ShaderStageFlagBits::eTessellationControl;
    case EShLangTessEvaluation:
        return vk::ShaderStageFlagBits::eTessellationEvaluation;
    case EShLangGeometry:
        return vk::ShaderStageFlagBits::eGeometry;
    case EShLangCompute:
        return vk::ShaderStageFlagBits::eCompute;
    case EShLangCount:
        return vk::ShaderStageFlagBits::eAll;
    }
}

static EShLanguage langForStage(vk::ShaderStageFlagBits stage) {
    switch (stage) {
    case vk::ShaderStageFlagBits::eVertex:
        return EShLangVertex;
    case vk::ShaderStageFlagBits::eFragment:
        return EShLangFragment;
    case vk::ShaderStageFlagBits::eTessellationControl:
        return EShLangTessControl;
    case vk::ShaderStageFlagBits::eTessellationEvaluation:
        return EShLangTessEvaluation;
    case vk::ShaderStageFlagBits::eGeometry:
        return EShLangGeometry;
    case vk::ShaderStageFlagBits::eCompute:
        return EShLangCompute;
    case vk::ShaderStageFlagBits::eAll:
    default:
        return EShLangCount;
    }
}

static std::string readFile(std::string const &filename) {
    std::ifstream in(filename);
    if (!in)
        throw std::runtime_error("File not found.");

    std::istreambuf_iterator<char> begin{in}, end;
    return {begin, end};
}

enum TOptions {
    EOptionNone = 0,
    EOptionIntermediate = (1 << 0),
    EOptionSuppressInfolog = (1 << 1),
    EOptionMemoryLeakMode = (1 << 2),
    EOptionRelaxedErrors = (1 << 3),
    EOptionGiveWarnings = (1 << 4),
    EOptionLinkProgram = (1 << 5),
    EOptionMultiThreaded = (1 << 6),
    EOptionDumpConfig = (1 << 7),
    EOptionDumpReflection = (1 << 8),
    EOptionSuppressWarnings = (1 << 9),
    EOptionDumpVersions = (1 << 10),
    EOptionSpv = (1 << 11),
    EOptionHumanReadableSpv = (1 << 12),
    EOptionVulkanRules = (1 << 13),
    EOptionDefaultDesktop = (1 << 14),
    EOptionOutputPreprocessed = (1 << 15),
    EOptionOutputHexadecimal = (1 << 16),
    EOptionReadHlsl = (1 << 17),
    EOptionCascadingErrors = (1 << 18),
    EOptionAutoMapBindings = (1 << 19),
    EOptionFlattenUniformArrays = (1 << 20),
    EOptionNoStorageFormat = (1 << 21),
    EOptionKeepUncalled = (1 << 22),
    EOptionHlslOffsets = (1 << 23),
    EOptionHlslIoMapping = (1 << 24),
    EOptionAutoMapLocations = (1 << 25),
    EOptionDebug = (1 << 26),
    EOptionStdin = (1 << 27),
    EOptionOptimizeDisable = (1 << 28),
    EOptionOptimizeSize = (1 << 29),
    EOptionInvertY = (1 << 30),
};

// Outputs the given string, but only if it is non-null and non-empty.
// This prevents erroneous newlines from appearing.
static void PutsIfNonEmpty(const char *str) {
    if (str && str[0]) {
        puts(str);
    }
}

// Outputs the given string to stderr, but only if it is non-null and non-empty.
// This prevents erroneous newlines from appearing.
static void StderrIfNonEmpty(const char *str) {
    if (str && str[0])
        fprintf(stderr, "%s\n", str);
}

static SharedShaderModule compileShader(Input const &input,
                                        SharedDevice const &device) {
    bool isGLSL = (input.entrypoint == "");
    if (!isGLSL)
        lava::info() << "Using experimental HLSL support.";
    auto lang = isGLSL ? findStage(input.filename) : langForStage(input.stage);

    std::unique_ptr<glslang::TShader> shader{new glslang::TShader(lang)};
    shader->setEnvInput(isGLSL ? glslang::EShSourceGlsl
                               : glslang::EShSourceHlsl,
                        lang, glslang::EShClientVulkan, 100);
    shader->setEnvClient(glslang::EShClientVulkan,
                         glslang::EShTargetClientVersion::EShTargetVulkan_1_0);
    shader->setEnvTarget(glslang::EShTargetSpv,
                         glslang::EShTargetLanguageVersion::EShTargetSpv_1_0);

    auto resources = glslang::DefaultTBuiltInResource;
    auto messages = EShMsgDefault;
    std::unique_ptr<DirStackFileIncluder> includer{new DirStackFileIncluder()};

    auto content = readFile(input.filename);

    auto contentptr = content.c_str();
    auto nameptr = input.filename.c_str();
    int lenghts = int(content.length());
    shader->setStringsWithLengthsAndNames(&contentptr, &lenghts, &nameptr, 1);

    if (!isGLSL)
        shader->setEntryPoint(input.entrypoint.c_str());

    std::string output;
    bool compiled = shader->parse(&resources, 450, ENoProfile, false, false,
                                  messages, *includer);

    std::string buildLog;
    buildLog.append(shader->getInfoLog());
    buildLog.append(shader->getInfoDebugLog());

    std::unique_ptr<glslang::TProgram> program{new glslang::TProgram()};
    program->addShader(shader.get());

    std::cerr << output << std::endl;

    bool linked = program->link(messages);
    buildLog.append(program->getInfoLog());
    buildLog.append(program->getInfoDebugLog());

    if (compiled && linked) {
        std::vector<uint32_t> spirv;
        spv::SpvBuildLogger logger;
        glslang::SpvOptions spvOptions;

        glslang::GlslangToSpv(*program->getIntermediate(lang), spirv, &logger,
                              &spvOptions);

        using charptr = char *;
        return device->createShader(charptr(spirv.data()),
                                    charptr(spirv.data() + spirv.size()));
    } else {
        throw ShaderCompileError(buildLog);
    }
}

SharedGraphicsPipeline
createPipeline(const Subpass &subpass,
               const GraphicsPipelineCreateInfo &createInfo,
               const std::vector<Input> &glslFiles) {
    auto enhancedCI = createInfo;
    auto const &device = subpass.pass->device();

    glslang::InitializeProcess();

    std::vector<DynamicShaderInfo> dynamicShaders;
    for (auto const &input : glslFiles) {
        auto module = compileShader(input, device);
        if (input.entrypoint.empty()) {
            // GLSL Shader
            auto lang = findStage(input.filename);
            enhancedCI.addStage(std::move(module), "main", stageForLang(lang));
        } else {
            // HLSL Shader
            enhancedCI.addStage(std::move(module), input.entrypoint,
                                input.stage);
        }
        dynamicShaders.push_back({input, enhancedCI.numStages() - 1,
                                  fileModificationTime(input.filename)});
    }

    auto pip = subpass.createPipeline(enhancedCI);
    {
        std::lock_guard<std::mutex> guard{sShaderInfosMutex};
        sShaderInfos.insert({pip, std::move(dynamicShaders)});
    }

    return pip;
}

static bool needsUpdate(DynamicShaderInfo const &info) {
    auto time = fileModificationTime(info.input.filename);
    return (info.modificationTime < time);
}

static bool needsUpdates(std::vector<DynamicShaderInfo> const &infos) {
    return std::any_of(begin(infos), end(infos), needsUpdate);
}

void reloadPipeline(SharedGraphicsPipeline &pipeline) {
    std::vector<DynamicShaderInfo> infos;
    {
        std::lock_guard<std::mutex> guard{sShaderInfosMutex};
        auto it = sShaderInfos.find(pipeline);
        if (it == sShaderInfos.end()) {
            lava::warning()
                << "Calling reloadPipeline() with a GraphicsPipeline "
                   "that was not created by lava-extras-glsl does "
                   "nothing.";
            sShaderInfos[pipeline]; // reduce spam by above warning
        } else {
            for (auto &i : it->second) {
                infos.push_back(i);
            }
        }
    }

    if (!needsUpdates(infos))
        return; // nothing to do

    try {
        auto ci = pipeline->createInfo();
        for (auto &info : infos) {
            auto time = fileModificationTime(info.input.filename);
            if (time <= info.modificationTime)
                continue;
            info.modificationTime = time;

            lava::info() << "Reloading shader " << info.input.filename;
            auto mod = compileShader(info.input, pipeline->device());
            ci.replaceStage(info.moduleIndex, mod);
        }

        pipeline = std::make_shared<GraphicsPipeline>(ci);
        {
            std::lock_guard<std::mutex> guard{sShaderInfosMutex};
            sShaderInfos.insert({pipeline, infos});

            for (auto it = begin(sShaderInfos); it != end(sShaderInfos); it++) {
                if (it->first.expired())
                    sShaderInfos.erase(it);
            }
        }
        lava::info() << "Rebuilt pipeline.";
    } catch (ShaderCompileError e) {
        lava::error() << "Could not reload shader:";
        lava::error() << e.what();
        { // Don't try and compile a broken shader again.
            std::lock_guard<std::mutex> guard{sShaderInfosMutex};
            sShaderInfos[pipeline] = infos;
        }
    }
}

ShaderCompileError::~ShaderCompileError() {}

Input GLSL(const std::string &filename) {
    Input result;
    result.filename = filename;
    result.stage = vk::ShaderStageFlagBits::eAll;
    return result;
}

Input HLSL(const std::string &filename, vk::ShaderStageFlagBits stage,
           const std::string &entrypoint) {
    Input result;
    result.filename = filename;
    result.stage = stage;
    result.entrypoint = entrypoint;
    return result;
}
}
}
