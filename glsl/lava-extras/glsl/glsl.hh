#pragma once
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>
#include <vector>
#include <string>

namespace lava {
namespace glsl {

struct Input {
    Input(const std::string& glslFile) : filename(glslFile) {}
    Input() = default;

    std::string filename;
    std::string entrypoint;
    vk::ShaderStageFlagBits stage;
};

/// GLSL files always have the entry point "main" and we can use the
/// extension to find the shader stage.
Input GLSL(const std::string &filename);

/// HLSL files can have multiple entry points for different stages in one
/// file
Input HLSL(const std::string &filename,
                  vk::ShaderStageFlagBits stage,
                  const std::string &entrypoint = "main");

/** This error is thrown by createPipeline if the shader could not be compiled.
 *  For reloads, this error will be caught internally and prevent the actual
 *  switching of the Pipeline.
 */
struct ShaderCompileError : public std::runtime_error {
    ShaderCompileError(std::string const &log) : std::runtime_error(log) {}
    ~ShaderCompileError();
};

/** Creates a Pipeline with the provided createInfos on the given device.
 *  Filenames given in glslFiles will be compiled from glsl.
 *  The shaders in glslFiles can be dynamically reloaded (this does not apply to
 *  the shader modules already present in createInfo.
 *
 *  Uses the file extension of the glslFile to determine the stage of the
 *  shader (.vert, .tesc, .tese, .geom, .frag).
 */
SharedGraphicsPipeline
createPipeline(Subpass const &subpass,
               GraphicsPipelineCreateInfo const &createInfo,
               std::vector<Input> const &glslFiles);

/** Checks if any of the glslFiles used to create the pipeline have changed.
 *  If so, it replaces the pipeline with a new, updated one.
 *  Requires that the pipeline has been created with the glsl::createPipeline()
 *  to work properly.
 *  This is safe to be used concurrently for different pipelines or with
 *  createPipeline.
 */
void reloadPipeline(SharedGraphicsPipeline &pipeline);
}
}
