#include "ForwardRenderingPipeline.hh"

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <lava/objects/Buffer.hh>
#include <lava/objects/DescriptorSet.hh>
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Framebuffer.hh>
#include <lava/objects/GraphicsPipeline.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/ImageView.hh>
#include <lava/objects/RenderPass.hh>
#include <lava/raii/ActiveRenderPass.hh>

#include <lava/createinfos/Buffers.hh>
#include <lava/createinfos/DescriptorSetLayoutCreateInfo.hh>
#include <lava/createinfos/Images.hh>
#include <lava/createinfos/PipelineShaderStageCreateInfo.hh>
#include <lava/createinfos/RenderPassCreateInfo.hh>
#include <lava/createinfos/Sampler.hh>

#include <lava-extras/pack/pack.hh>

using namespace lava;
using namespace lava::pipeline;

ForwardRenderingPipeline::ForwardRenderingPipeline(const SharedDevice &device,
                                                   GenericFormat outputFormat)
    : mDevice(device), mOutputFormat(outputFormat.vkhpp()) {

    {
        auto info = lava::RenderPassCreateInfo{};

        info.addAttachment(AttachmentDescription::depth32float()
                               .clear()
                               .finalLayout_Attachment());

        info.addAttachment(AttachmentDescription::color(Format::RGBA16F)
                               .clear()
                               .finalLayout_ShaderRead());

        info.addSubpass(SubpassDescription{}.depth(0).colors({1}));

        mPassForward = mDevice->createRenderPass(info);
    }

    {
        auto info = lava::RenderPassCreateInfo{};
        info.addAttachment(AttachmentDescription::color(mOutputFormat)
                               .clear()
                               .finalLayout_PresentSrc());
        info.addDependency(SubpassDependency::first().sampleColor());
        info.addSubpass(SubpassDescription{}.colors({0}));

        mPassOutput = mDevice->createRenderPass(info);
    }

    {
        auto dsinfo = DescriptorSetLayoutCreateInfo{};
        dsinfo.addBinding(vk::DescriptorType::eCombinedImageSampler,
                          vk::ShaderStageFlagBits::eFragment, 1);
        mOutputDescriptorLayout = mDevice->createDescriptorSetLayout(dsinfo);

        auto layout =
            mDevice->createPipelineLayout({}, {mOutputDescriptorLayout});

        auto info = lava::GraphicsPipelineCreateInfo::defaults();
        info.setLayout(layout);
        info.addStage(
            pack::shader(mDevice, "lava-extras/pipeline/output.vert"));
        info.addStage(
            pack::shader(mDevice, "lava-extras/pipeline/output.frag"));

        info.stage(1).specialize(0, false); // Disable FXAA
        mPipelineOutputNoFXAA = mPassOutput->createPipeline(0, info);

        info.stage(1).specialize(0, true); // Enable FXAA
        mPipelineOutputFXAA = mPassOutput->createPipeline(0, info);
    }

    {
        mViewProjBuffer = mDevice->createBuffer(
            lava::uniformBuffer(sizeof(ViewProjMatrixData)));

        mViewProjBuffer->keepStagingBuffer();
        mViewProjBuffer->realizeVRAM();

        auto dslinfo = DescriptorSetLayoutCreateInfo{};
        dslinfo.addUniformBuffer(vk::ShaderStageFlagBits::eAllGraphics);

        mViewProjDescriptorSetLayout =
            mDevice->createDescriptorSetLayout(dslinfo);

        mViewProjDescriptor =
            mViewProjDescriptorSetLayout->createDescriptorSet();
        mViewProjDescriptor->writeUniformBuffer(mViewProjBuffer, 0);
    }
}

void ForwardRenderingPipeline::resize(int w, int h) {
    if (w == mWidth && h == mHeight)
        return; // no change

    mWidth = w;
    mHeight = h;

    mImageColor =
        lava::attachment2D(mWidth, mHeight, Format::RGBA16F).create(mDevice);
    mImageColor->realizeAttachment();
    mViewColor = mImageColor->createView();

    mImageDepth =
        lava::attachment2D(mWidth, mHeight, Format::DEPTH_COMPONENT32F)
            .create(mDevice);
    mImageDepth->realizeAttachment();
    mViewDepth = mImageDepth->createView();

    mFboForward = mPassForward->createFramebuffer({mViewDepth, mViewColor});

    auto sampler = mDevice->createSampler(
        SamplerCreateInfo{}
            .setUnnormalizedCoordinates(true)
            .setMipmapMode(vk::SamplerMipmapMode::eNearest)
            .setMaxLod(0)
            .setAddressModeV(vk::SamplerAddressMode::eClampToEdge)
            .setAddressModeU(vk::SamplerAddressMode::eClampToEdge));


    mOutputDescriptor = mOutputDescriptorLayout->createDescriptorSet();
    mOutputDescriptor->writeCombinedImageSampler({sampler, mViewColor}, 0);
}

void ForwardRenderingPipeline::updateViewProjMatrices(
    RecordingCommandBuffer &cmd, glm::mat4 view, glm::mat4 proj) {
    mView = view;
    mProj = proj;

    ViewProjMatrixData matrixData{view, proj};

    mViewProjBuffer->setDataVRAM(&matrixData, sizeof(matrixData), cmd);
}

void ForwardRenderingPipeline::render(
    lava::RecordingCommandBuffer &cmd, lava::SharedFramebuffer const &fbo,
    std::function<void(lava::pipeline::RenderPassSimple const &pass)> const
        &renderFunc) {

    assert(renderFunc != nullptr && "no render function provided");

    {
        auto pass = cmd.beginRenderpass(mFboForward);

        {
            RenderPassSimple rp{RenderPassType::Opaque, this, mFboForward,
                                pass};

            renderFunc(rp);
        }
    }

    {
        auto pass = cmd.beginRenderpass(fbo);
        auto sub = pass.startInlineSubpass();

        sub.bindPipeline(mFXAA ? mPipelineOutputFXAA : mPipelineOutputNoFXAA);

        sub.bindDescriptorSets({mOutputDescriptor});

        sub.draw(3);
    }
}

const SharedDescriptorSet &
ForwardRenderingPipeline::getViewProjDescriptor() const {
    return mViewProjDescriptor;
}

const SharedDescriptorSetLayout &
ForwardRenderingPipeline::getViewProjDescriptorSetLayout() const {
    return mViewProjDescriptorSetLayout;
}
