#pragma once

#include <vector>
#include <functional>

#include "RenderPass.hh"

#include <lava/fwd.hh>
#include <lava/common/property.hh>
#include <lava/common/GlLikeFormats.hh>
#include <lava-extras/camera/fwd.hh>
#include <lava-extras/pipeline/fwd.hh>
#include <lava/objects/RenderPass.hh>

#include <glm/vec3.hpp>

namespace lava {

namespace pipeline {
/**
 * A Forward+ Rendering Pipeline with post-processing support
 * (at some point in the future)
 *
 * Features (at some point in the future):
 *  * Tiled Light Rendering
 *  * Z-PrePass
 *  * Soft Shadows
 *  * Inverse Depth Buffer
 *  * Dithering
 *  * HDR
 *
 * Basic usage:
 *   // setup shader path
 *   DefaultShaderParser::addIncludePath("PATH/TO/glow-extras/pipeline/shader");
 *
 *   // creation
 *   pipeline = RenderingPipeline::create(myCamera);
 *
 *   // configuration
 *   pipeline->setXYZ(...); // optional
 *
 *   // rendering
 *   pipeline->updateShadows(...); // optional
 *   pipeline->render(myRenderFunc);
 *
 * Transparency:
 *   This pipeline implements Weighted Blended Order-Independent Transparency
 *(http://jcgt.org/published/0002/02/09/)
 *   Use #include <glow-pipeline/transparency.glsl> (that file includes further
 *documentation)
 *
 */
class RenderingPipeline {
  private:
    /// Generic Camera used for rendering
    camera::SharedGenericCamera mCamera = nullptr;

    /// Render width in px
    int mWidth = 2;
    /// Render height in px
    int mHeight = 2;

    // === RENDER TARGETS ===
    SharedImage mImageNormals;
    SharedImage mImageColor;
    SharedImage mImageColorTmp;
    SharedImage mImageTranspAccum;
    SharedImage mImageTranspReveal;
    SharedImage mImageDepth;
    SharedImage mImageDepthPre;
    SharedImage mImageSSAO;

    SharedImageView mViewNormals;
    SharedImageView mViewColor;
    SharedImageView mViewColorTmp;
    SharedImageView mViewTranspAccum;
    SharedImageView mViewTranspReveal;
    SharedImageView mViewDepth;
    SharedImageView mViewDepthPre;
    SharedImageView mViewSSAO;

    // === Framebuffers ===
    SharedFramebuffer mFboZPreForward;
    SharedFramebuffer mFboToColor;
    SharedFramebuffer mFboToColorTmp;
    SharedFramebuffer mFboTransparent;

    // === (Vulkan) RenderPasses ===
    SharedRenderPass mPassZPreForward; // 2 subpasses
    SharedRenderPass mPassOutput;

    // === Pipelines ===
    SharedGraphicsPipeline mPipelineOutputFXAA;
    SharedGraphicsPipeline mPipelineOutputNoFXAA;
    SharedGraphicsPipeline mPipelineFXAA;
    SharedGraphicsPipeline mPipelineToneMap;
    SharedGraphicsPipeline mPipelineAlphaResolve;

    // === Descriptor Sets
    SharedDescriptorSetLayout mOutputDescriptorLayout;
    SharedDescriptorSet mOutputDescriptor;

    // === SETTINGS ===
    bool mFXAA = true;
    bool mTransparentPass = true;
    float mDitheringStrength = 1 / 256.f;
    glm::vec3 mClearColor;

    SharedDevice mDevice;
    vk::Format mOutputFormat;

  public: // getter, setter
    LAVA_PROPERTY(Camera);

    LAVA_GETTER(Width);
    LAVA_GETTER(Height);

    LAVA_PROPERTY(FXAA);
    LAVA_PROPERTY(DitheringStrength);
    LAVA_PROPERTY(TransparentPass);
    LAVA_PROPERTY(ClearColor);

    int getOutputWidth() const;
    int getOutputHeight() const;

  public:
    RenderingPipeline(SharedDevice const &device, GenericFormat outputFormat);

    /// Resizes the internal pipeline size
    void resize(int w, int h);

    /// Updates the camera
    /// If useCamViewport is true, also resizes the pipeline
    void assignCamera(camera::SharedGenericCamera const &cam,
                      bool useCamViewport = true);

    /**
     * @brief executes the whole rendering pipeline
     * @param renderFunc function to call for every renderpass. Use the provided
     * pass information
     */
    void render(lava::RecordingCommandBuffer &cmd,
                lava::SharedFramebuffer const &fbo,
                const std::function<
                    void(lava::pipeline::RenderPass const &pass)> &renderFunc);

    /// Use this to create the FBOs for the Pipeline to output to
    SharedRenderPass const &outputPass() const { return mPassOutput; }

    /// Use this to create the Pipelines to render the forward pass
    SharedRenderPass const &zPreForwardPass() const { return mPassZPreForward; }

    Subpass zPrePass() const { return {mPassZPreForward, 0}; }
    Subpass opaquePass() const { return {mPassZPreForward, 1}; }

  public:
    /// Reads back part of the depth buffer and calculates the 3d position of
    /// the pixel
    /// returns false if depth is background (== 1)
    /// (x,y) are in camera pixel coords (they'll get scaled if internal size
    /// differs)
    /// (0,0) is top-left
    bool queryPosition3D(int x, int y, glm::vec3 *pos, float *depth = nullptr);
};
}
}
