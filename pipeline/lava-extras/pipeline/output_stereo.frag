#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_GOOGLE_include_directive : enable

#include "fxaa.glsl"

layout (binding = 0) uniform sampler2DArray uColor;

layout (location = 0) out vec3 fColor;

layout (constant_id = 0) const bool useFXAA = false;

layout(push_constant) uniform PushConsts {
    uint eye;
} pu;

void main() {
    if (useFXAA) {
        fColor = fxaaArray(uColor, vec3(gl_FragCoord.xy, float(pu.eye))).rgb;
    } else {
        fColor = texture(uColor, vec3(gl_FragCoord.xy, float(pu.eye))).rgb;
    }
}
