#include "RenderingPipeline.hh"

#include <glm/glm.hpp>

#include <lava/objects/DescriptorSet.hh>
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Framebuffer.hh>
#include <lava/objects/GraphicsPipeline.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/ImageView.hh>
#include <lava/objects/RenderPass.hh>
#include <lava/raii/ActiveRenderPass.hh>

#include <lava/createinfos/Images.hh>
#include <lava/createinfos/PipelineShaderStageCreateInfo.hh>
#include <lava/createinfos/RenderPassCreateInfo.hh>
#include <lava/createinfos/Sampler.hh>

#include <lava-extras/camera/FixedCamera.hh>
#include <lava-extras/camera/GenericCamera.hh>

#include <glm/ext.hpp>
#include <lava/createinfos/DescriptorSetLayoutCreateInfo.hh>

#include <lava-extras/pack/pack.hh>

using namespace lava;
using namespace lava::pipeline;

int RenderingPipeline::getOutputWidth() const {
    return mCamera ? mCamera->getViewportWidth() : mWidth;
}

int RenderingPipeline::getOutputHeight() const {
    return mCamera ? mCamera->getViewportHeight() : mHeight;
}

RenderingPipeline::RenderingPipeline(const SharedDevice &device,
                                     GenericFormat outputFormat)
    : mDevice(device), mOutputFormat(outputFormat.vkhpp()) {

    mCamera = std::make_shared<camera::GenericCamera>();
    mCamera->setPosition({0.f, 0.f, 0.f});
    mCamera->setTarget({0.f, 0.f, -1.f});

    mClearColor = glm::pow(glm::vec3(0 / 255.f, 84 / 255.f, 159 / 255.f),
                           glm::vec3(2.2f));

    {
        auto info = lava::RenderPassCreateInfo{};
        info.addAttachment(AttachmentDescription::depth32float()
                               .clear()
                               .finalLayout_Attachment());
        info.addAttachment(AttachmentDescription::color(Format::RGBA16F)
                               .clear()
                               .finalLayout_ShaderRead());
        info.addDependency(SubpassDependency(0, 1).reuseDepthStencil());
        info.addSubpass(SubpassDescription{}.depth(0));
        info.addSubpass(SubpassDescription{}.depth(0).colors({1}));

        mPassZPreForward = mDevice->createRenderPass(info);
    }

    {
        auto info = lava::RenderPassCreateInfo{};
        info.addAttachment(AttachmentDescription::color(mOutputFormat)
                               .clear()
                               .finalLayout_PresentSrc());
        info.addDependency(SubpassDependency::first().sampleColor());
        info.addSubpass(SubpassDescription{}.colors({0}));

        mPassOutput = mDevice->createRenderPass(info);
    }
    {
        auto dsinfo = DescriptorSetLayoutCreateInfo{};
        dsinfo.addBinding(vk::DescriptorType::eCombinedImageSampler,
                          vk::ShaderStageFlagBits::eFragment, 1);
        mOutputDescriptorLayout = mDevice->createDescriptorSetLayout(dsinfo);

        auto layout =
            mDevice->createPipelineLayout({}, {mOutputDescriptorLayout});

        auto info = lava::GraphicsPipelineCreateInfo::defaults();
        info.setLayout(layout);
        info.addStage(
            pack::shader(mDevice, "lava-extras/pipeline/output.vert"));
        info.addStage(
            pack::shader(mDevice, "lava-extras/pipeline/output.frag"));

        info.stage(1).specialize(0, false); // Disable FXAA
        mPipelineOutputNoFXAA = mPassOutput->createPipeline(0, info);

        info.stage(1).specialize(0, true); // Enable FXAA
        mPipelineOutputFXAA = mPassOutput->createPipeline(0, info);
    }
}

void RenderingPipeline::resize(int w, int h) {
    if (w == mWidth && h == mHeight)
        return; // no change

    mDevice->handle().waitIdle();

    mWidth = w;
    mHeight = h;

    mImageNormals =
        lava::attachment2D(mWidth, mHeight, Format::RGBA8).create(mDevice);
    mImageNormals->realizeAttachment();
    mViewNormals = mImageNormals->createView();

    mImageColor =
        lava::attachment2D(mWidth, mHeight, Format::RGBA16F).create(mDevice);
    mImageColor->realizeAttachment();
    mViewColor = mImageColor->createView();

    mImageColorTmp =
        lava::attachment2D(mWidth, mHeight, Format::RGBA16F).create(mDevice);
    mImageColorTmp->realizeAttachment();
    mViewColorTmp = mImageColorTmp->createView();

    mImageTranspAccum =
        lava::attachment2D(mWidth, mHeight, Format::RGBA16F).create(mDevice);
    mImageTranspAccum->realizeAttachment();
    mViewTranspAccum = mImageTranspAccum->createView();

    mImageTranspReveal =
        lava::attachment2D(mWidth, mHeight, Format::R16F).create(mDevice);
    mImageTranspReveal->realizeAttachment();
    mViewTranspReveal = mImageTranspReveal->createView();

    mImageSSAO =
        lava::attachment2D(mWidth, mHeight, Format::R16F).create(mDevice);
    mImageSSAO->realizeAttachment();
    mViewSSAO = mImageSSAO->createView();

    mImageDepth =
        lava::attachment2D(mWidth, mHeight, Format::DEPTH_COMPONENT32F)
            .create(mDevice);
    mImageDepth->realizeAttachment();
    mViewDepth = mImageDepth->createView();

    mImageDepthPre =
        lava::attachment2D(mWidth, mHeight, Format::DEPTH_COMPONENT32F)
            .create(mDevice);
    mImageDepthPre->realizeAttachment();
    mViewDepthPre = mImageDepthPre->createView();

    mFboZPreForward =
        mPassZPreForward->createFramebuffer({mViewDepth, mViewColor});

    auto sampler = mDevice->createSampler(
        SamplerCreateInfo{}.setUnnormalizedCoordinates(true));

    mOutputDescriptor = mOutputDescriptorLayout->createDescriptorSet();
    mOutputDescriptor->writeCombinedImageSampler({sampler, mViewColor}, 0);
}

void RenderingPipeline::assignCamera(const camera::SharedGenericCamera &cam,
                                     bool useCamViewport) {
    mCamera = cam;

    if (cam && useCamViewport)
        resize(cam->getViewportWidth(), cam->getViewportHeight());
}

void RenderingPipeline::render(
    lava::RecordingCommandBuffer &cmd, lava::SharedFramebuffer const &fbo,
    std::function<void(lava::pipeline::RenderPass const &pass)> const
        &renderFunc) {

    assert(renderFunc != nullptr && "no render function provided");
    assert(mCamera != nullptr && "no camera provided");

    camera::FixedCamera cam(mCamera->getPosition(),         //
                            mCamera->getViewMatrix(),       //
                            mCamera->getProjectionMatrix(), //
                            mCamera->getViewportSize());

    {
        auto pass = cmd.beginRenderpass(mFboZPreForward);

        // z-Pre
        {
            RenderPass rp{RenderPassType::ZPre, this, cam, mFboZPreForward,
                          pass};
            renderFunc(rp);
        }

        {
            RenderPass rp{RenderPassType::Opaque, this, cam, mFboZPreForward,
                          pass};
            renderFunc(rp);
        }
    }

    {
        auto pass = cmd.beginRenderpass(fbo);
        auto sub = pass.startInlineSubpass();

        sub.bindPipeline(mFXAA ? mPipelineOutputFXAA : mPipelineOutputNoFXAA);
        sub.bindDescriptorSets({mOutputDescriptor});

        sub.draw(3);
    }
}

bool RenderingPipeline::queryPosition3D(int x, int y, glm::vec3 *pos,
                                        float *depth) {
    /*if (!mCamera)
        return false;
    if (x < 0 || y < 0 || x >= (int)mCamera->getViewportWidth() || y >=
    (int)mCamera->getViewportHeight())
        return false;

    // rescale position
    x = x * mWidth / mCamera->getViewportWidth();
    y = mHeight - y * mHeight / mCamera->getViewportHeight() - 1;

    auto fb = mFboForward->bind();
    float d;

    glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &d);
    if (depth)
        *depth = d;

    if (d < 1)
    {
        // unproject (with viewport coords!)
        glm::vec4 v{x / float(mWidth) * 2 - 1, y / float(mHeight) * 2 - 1, d * 2
    - 1, 1.0};

        v = glm::inverse(mCamera->getProjectionMatrix()) * v;
        v /= v.w;
        v = glm::inverse(mCamera->getViewMatrix()) * v;
        if (pos)
            *pos = glm::vec3(v);

        return true;
    }*/

    return false;
}
