#include "StereoPipeline.hh"

#include <lava-extras/pack/pack.hh>
#include <lava/createinfos/Buffers.hh>
#include <lava/createinfos/DescriptorSetLayoutCreateInfo.hh>
#include <lava/createinfos/GraphicsPipelineCreateInfo.hh>
#include <lava/createinfos/Images.hh>
#include <lava/createinfos/RenderPassCreateInfo.hh>
#include <lava/createinfos/RenderPassMultiviewCreateInfoKHX.hh>
#include <lava/createinfos/Sampler.hh>
#include <lava/features/DebugMarkers.hh>
#include <lava/features/MultiView.hh>
#include <lava/objects/Buffer.hh>
#include <lava/objects/CommandBuffer.hh>
#include <lava/objects/DescriptorSet.hh>
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Framebuffer.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/ImageView.hh>
#include <lava/objects/Instance.hh>
#include <lava/objects/RenderPass.hh>
#include <lava/raii/ActiveRenderPass.hh>

namespace lava {
namespace pipeline {

template <class T, size_t N, class F>
static void fill(std::array<T, N> &target, F &&func) {
    for (size_t i = 0; i < N; ++i)
        target[i] = func(i);
}

StereoPipeline::StereoPipeline(const SharedDevice &device, uint32_t width,
                               uint32_t height,
                               StereoPipeline::Settings settings)
    : mDevice(device), mSettings(settings) {

    assert(mDevice->get<features::MultiView>() &&
           "The device needs to have the MultiView feature active.");

    fill(mEyes, [this](size_t) {
        camera::FixedCamera result;
        result.setPosition({0, 0, 1});
        result.setViewMatrix(glm::mat4x4{1.f});
        result.setProjectionMatrix(glm::mat4x4{1.f});
        return result;
    });

    {
        auto info = RenderPassCreateInfo{};
        info.addAttachment(AttachmentDescription::depth32float()
                               .clear()
                               .finalLayout_Attachment());

        info.addAttachment(AttachmentDescription::color(Format::SRGB8_ALPHA8)
                               .clear()
                               .finalLayout_ShaderRead());

        if (mSettings.useZPrePass) {
            info.addDependency(SubpassDependency(0, 1).reuseDepthStencil());
            info.addSubpass(SubpassDescription{}.depth(0).perViewAttributes(
                mSettings.usePerViewAttributes));
        }
        info.addSubpass(
            SubpassDescription{}.depth(0).colors({1}).perViewAttributes(
                mSettings.usePerViewAttributes));

        auto mv = std::make_shared<RenderPassMultiviewCreateInfoKHX>(info, 2);
        mv->addCorrelation(3);
        info.setNext(mv);

        mPassZPreForward = mDevice->createRenderPass(info);
    }

    {
        auto info = lava::RenderPassCreateInfo{};
        info.addAttachment(AttachmentDescription::color(Format::SRGB8_ALPHA8)
                               .clear()
                               .finalLayout_TransferSrc());
        info.addDependency(SubpassDependency::first().sampleColor());
        info.addSubpass(SubpassDescription{}.colors({0}));

        mPassOutput = mDevice->createRenderPass(info);
    }
    {
        auto dsinfo = DescriptorSetLayoutCreateInfo{};
        dsinfo.addBinding(vk::DescriptorType::eCombinedImageSampler,
                          vk::ShaderStageFlagBits::eFragment, 1);
        mOutputDescriptorLayout = mDevice->createDescriptorSetLayout(dsinfo);

        auto layout =
            mDevice->createPipelineLayout<EyeIndex>({mOutputDescriptorLayout});

        auto info = lava::GraphicsPipelineCreateInfo::defaults();
        info.setLayout(layout);
        info.addStage(
            pack::shader(mDevice, "lava-extras/pipeline/output.vert"));
        info.addStage(
            pack::shader(mDevice, "lava-extras/pipeline/output_stereo.frag"));

        info.stage(1).specialize(0, false); // Disable FXAA
        mPipelineOutput = mPassOutput->createPipeline(0, info);

        info.stage(1).specialize(0, true); // Enable FXAA
        mPipelineOutputFxaa = mPassOutput->createPipeline(0, info);
    }
    {
        mCamerasBuffer =
            mDevice->createBuffer(lava::uniformBuffer(sizeof(CameraData)));
        mCamerasBuffer->keepStagingBuffer();
        mCamerasBuffer->realizeVRAM();

        auto dslinfo = DescriptorSetLayoutCreateInfo{};
        dslinfo.addUniformBuffer(vk::ShaderStageFlagBits::eAllGraphics);

        mCamerasDescriptorSetLayout =
            mDevice->createDescriptorSetLayout(dslinfo);

        mCamerasDescriptor = mCamerasDescriptorSetLayout->createDescriptorSet();
        mCamerasDescriptor->writeUniformBuffer(mCamerasBuffer, 0);
    }

    if (width && height)
        resize(width, height);
}

void StereoPipeline::render(
    RecordingCommandBuffer &cmd,
    const std::function<void(RenderPassStereo const &)> &renderFunc) {
    assert(renderFunc != nullptr && "no render function provided");

    {
        auto pass = cmd.beginRenderpass(mFboZPreForward);

        // z-Pre
        if (mSettings.useZPrePass) {
            RenderPassStereo rp{RenderPassType::ZPre, this, mEyes,
                                mFboZPreForward, pass};
            renderFunc(rp);
        }

        {
            RenderPassStereo rp{RenderPassType::Opaque, this, mEyes,
                                mFboZPreForward, pass};
            renderFunc(rp);
        }
    }

    for (size_t i = 0; i < 2; ++i) {
        auto pass = cmd.beginRenderpass(mFboOutput[i]);
        auto sub = pass.startInlineSubpass();

        sub.setViewports(Viewport(mWidth, mHeight));
        if (mFxaa)
            sub.bindPipeline(mPipelineOutputFxaa);
        else
            sub.bindPipeline(mPipelineOutput);
        sub.pushConstantBlock<EyeIndex>(EyeIndex(i));
        sub.bindDescriptorSets({mOutputDescriptor});

        sub.draw(3);
    }
}

const SharedDescriptorSet &StereoPipeline::getCameraDescriptor() const {
    return mCamerasDescriptor;
}

const SharedDescriptorSetLayout &
StereoPipeline::getCameraDescriptorSetLayout() const {
    return mCamerasDescriptorSetLayout;
}
void StereoPipeline::resize(uint32_t w, uint32_t h) {
    if (w == mWidth && h == mHeight)
        return; // no change

    auto debug = mDevice->get<features::DebugMarkers>();

    mWidth = w;
    mHeight = h;

    mEyes[0].setViewportSize({mWidth, mHeight});
    mEyes[1].setViewportSize({mWidth, mHeight});

    mImageColor =
        lava::attachment2DArray(mWidth, mHeight, 2, Format::SRGB8_ALPHA8)
            .create(mDevice);
    mImageColor->realizeAttachment();
    if (debug)
        debug->mark(mImageColor, "StereoPipeline - ColorAttachment");

    mImageDepth =
        lava::attachment2DArray(mWidth, mHeight, 2, Format::DEPTH_COMPONENT32F)
            .create(mDevice);
    mImageDepth->realizeAttachment();
    if (debug)
        debug->mark(mImageDepth, "StereoPipeline - DepthAttachment");

    mViewColor = mImageColor->createView();
    mViewDepth = mImageDepth->createView();

    mFboZPreForward =
        mPassZPreForward->createFramebuffer({mViewDepth, mViewColor});

    fill(mImageOutput, [&, this](size_t i) {
        auto image = lava::attachment2D(mWidth, mHeight, Format::SRGB8_ALPHA8)
                         .create(mDevice);
        if (debug)
            debug->mark(image,
                        "StereoPipeline - OutputImage " + std::to_string(i));
        image->realizeAttachment();
        return image;
    });

    fill(mViewOutput,
         [this](size_t i) { return mImageOutput[i]->createView(); });

    fill(mFboOutput, [this](size_t i) {
        return mPassOutput->createFramebuffer({mViewOutput[i]});
    });

    auto sampler = mDevice->createSampler(
        SamplerCreateInfo{}
            .setUnnormalizedCoordinates(true)
            .setMipmapMode(vk::SamplerMipmapMode::eNearest)
            .setMaxLod(0)
            .setAddressModeV(vk::SamplerAddressMode::eClampToEdge)
            .setAddressModeU(vk::SamplerAddressMode::eClampToEdge));


    mOutputDescriptor = mOutputDescriptorLayout->createDescriptorSet();
    mOutputDescriptor->writeCombinedImageSampler({sampler, mViewColor}, 0);
}

void lava::pipeline::StereoPipeline::updateEyes(
    RecordingCommandBuffer &cmd,
    const std::array<StereoPipeline::EyeData, 2> &data) {

    mEyes[0].setViewMatrix(data[0].view);
    mEyes[0].setProjectionMatrix(data[0].proj);
    mEyes[1].setViewMatrix(data[1].view);
    mEyes[1].setProjectionMatrix(data[1].proj);

    CameraData cams;
    cams.view[0] = mEyes[0].getViewMatrix();
    cams.view[1] = mEyes[1].getViewMatrix();
    cams.proj[0] = mEyes[0].getProjectionMatrix();
    cams.proj[1] = mEyes[1].getProjectionMatrix();

    mCamerasBuffer->setDataVRAM(&cams, sizeof(CameraData), cmd);
}
} // namespace pipeline
} // namespace lava
