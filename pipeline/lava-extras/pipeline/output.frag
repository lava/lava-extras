#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_GOOGLE_include_directive : enable

#include "fxaa.glsl"

layout (binding = 0) uniform sampler2D uColor;

layout (location = 0) out vec3 fColor;

layout (constant_id = 0) const bool useFXAA = true;

void main() {
    if (useFXAA) {
        fColor = fxaa(uColor, gl_FragCoord.xy).rgb;
    } else {
        fColor = texture(uColor, gl_FragCoord.xy).rgb;
    }
}
