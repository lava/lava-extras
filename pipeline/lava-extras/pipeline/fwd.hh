#pragma once
#include <memory>

#define LAVA_FORWARD_DECLARE_CLASS(T)                                          \
    class T;                                                                   \
    using Shared##T = std::shared_ptr<T>;                                      \
    using Weak##T = std::weak_ptr<T>;                                          \
    using Unique##T = std::unique_ptr<T>

namespace lava {
namespace pipeline {

LAVA_FORWARD_DECLARE_CLASS(RenderingPipeline);
LAVA_FORWARD_DECLARE_CLASS(StereoPipeline);
struct RenderPass;
} // namespace pipeline
} // namespace lava

#undef LAVA_FORWARD_DECLARE_CLASS
