#pragma once

#include "RenderPassType.hh"

#include <lava/fwd.hh>
#include <lava-extras/camera/FixedCamera.hh>
#include <array>

namespace lava {
namespace camera {
class FixedCamera;
}

class ActiveRenderPass;

namespace pipeline {
class RenderingPipeline;
class StereoPipeline;
class ForwardRenderingPipeline;

/**
 * @brief Information about the current render pass
 *
 * CAREFUL: This data is temporary and may not be valid after the rendering
 *function returns
 */
struct RenderPass {
    /// Type of the current pass (there can be multiple passes with the same
    /// type)
    RenderPassType type;

    /// Backreference to current pipeline
    RenderingPipeline *pipeline;

    /// Camera parameters (including width/height) for current pass
    camera::FixedCamera camera;

    /// Framebuffer to render to
    SharedFramebuffer framebuffer;

    /// The current RenderPass with which to draw. The renderfunc is responsible
    /// for starting a subpass (inline or secondary) itself
    ActiveRenderPass &pass;
};


/**
 * @brief Information about the current render pass
 *
 * CAREFUL: This data is temporary and may not be valid after the rendering
 *function returns
 */
struct RenderPassSimple {
    /// Type of the current pass (there can be multiple passes with the same
    /// type)
    RenderPassType type;

    /// Backreference to current pipeline
    ForwardRenderingPipeline *pipeline;

    /// Framebuffer to render to
    SharedFramebuffer framebuffer;

    /// The current RenderPass with which to draw. The renderfunc is responsible
    /// for starting a subpass (inline or secondary) itself
    ActiveRenderPass &pass;
};


/**
 * @brief Information about the current render pass
 *
 * CAREFUL: This data is temporary and may not be valid after the rendering
 *function returns
 */
struct RenderPassStereo {
    /// Type of the current pass (there can be multiple passes with the same
    /// type)
    RenderPassType type;

    /// Backreference to current pipeline
    StereoPipeline *pipeline;

    /// Camera parameters (including width/height) for current pass
    std::array<camera::FixedCamera,2> camera;

    /// Framebuffer to render to
    SharedFramebuffer framebuffer;

    /// The current RenderPass with which to draw. The renderfunc is responsible
    /// for starting a subpass (inline or secondary) itself
    ActiveRenderPass &pass;
};



}
}
