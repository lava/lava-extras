#pragma once
#include <lava/fwd.hh>
#include <array>
#include <lava-extras/camera/FixedCamera.hh>
#include "RenderPass.hh"
#include <lava/objects/RenderPass.hh>
#include <functional>

namespace lava {
namespace pipeline {

/**
 * The StereoPipeline uses the MultiView KHR extension to provide stereo
 * rendering (usually for VR applications).
 */

class StereoPipeline {
  public:
    struct Settings {
        bool usePerViewAttributes;
        bool useZPrePass;

        Settings& setPerViewAttributes(bool val) { usePerViewAttributes = val; return *this; }
        Settings& setZPrePass(bool val) { useZPrePass = val; return * this;}
    };

    struct EyeData {
        glm::mat4x4 view;
        glm::mat4x4 proj;
    };

    /// Separate from EyeData. This will be the format of the Camera UBO.
    /// This struct is subject to change for e.g. Lens-Matched-Shading
    struct CameraData {
        glm::mat4x4 view[2];
        glm::mat4x4 proj[2];
    };

    StereoPipeline(SharedDevice const &device, uint32_t width = 0,
                   uint32_t height = 0, Settings settings = {false, true});

    using Subpass = lava::Subpass;

    void resize(uint32_t w, uint32_t h);

    enum class EyeIndex { left, right };
    void setEye(EyeIndex index, camera::FixedCamera const &cam) {
        mEyes[size_t(index)] = cam;
        mEyes[size_t(index)].setViewportSize({mWidth, mHeight});
    }

    void updateEyes(RecordingCommandBuffer &cmd,
                    std::array<EyeData, 2> const &data);

    void
    render(RecordingCommandBuffer &cmd,
           const std::function<void(const RenderPassStereo &)> &renderFunc);

    SharedImage const &getOutput(EyeIndex eye) {
        return mImageOutput[size_t(eye)];
    }

    Subpass zPrePass() const { return {mPassZPreForward, 0}; }
    Subpass forwardPass() const { return {mPassZPreForward, mSettings.useZPrePass ? 1u : 0}; }

    SharedBuffer const &getCameraBuffer() const { return mCamerasBuffer; }
    SharedDescriptorSet const &getCameraDescriptor() const;
    SharedDescriptorSetLayout const& getCameraDescriptorSetLayout() const;

    uint32_t width() const { return mWidth; }
    uint32_t height() const { return mHeight; }

  protected:
    uint32_t mWidth = 0, mHeight = 0;
    bool mFxaa = true;
    Settings mSettings;

    SharedDevice mDevice;

    SharedImage mImageDepth;
    SharedImage mImageColor;

    SharedImageView mViewDepth;
    SharedImageView mViewColor;

    SharedRenderPass mZPreForward;
    SharedFramebuffer mFboZPreForward;
    SharedRenderPass mPassZPreForward, mPassOutput;

    std::array<SharedImage, 2> mImageOutput;
    std::array<SharedImageView, 2> mViewOutput;
    std::array<SharedFramebuffer, 2> mFboOutput;

    SharedDescriptorSetLayout mOutputDescriptorLayout;
    SharedDescriptorSet mOutputDescriptor;

    SharedGraphicsPipeline mPipelineOutput, mPipelineOutputFxaa;

    SharedBuffer mCamerasBuffer;
    SharedDescriptorSetLayout mCamerasDescriptorSetLayout;
    SharedDescriptorSet mCamerasDescriptor;

    std::array<camera::FixedCamera, 2> mEyes;
};
}
}
