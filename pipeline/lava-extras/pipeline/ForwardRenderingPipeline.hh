#pragma once

#include <functional>

#include "RenderPass.hh"

#include <lava/common/property.hh>
#include <lava/objects/RenderPass.hh>


namespace lava {

struct ViewProjMatrixData {
    glm::mat4x4 view;
    glm::mat4x4 proj;
};

namespace pipeline {
/**
 * A minimal Forward Rendering Pipeline with support for FXAA.
 *
 *
 * Basic usage:
 *   // setup shader path
 *   DefaultShaderParser::addIncludePath("PATH/TO/glow-extras/pipeline/shader");
 *
 *   // creation
 *   pipeline = new SimplePipeline(...);
 *   pipeline->resize(...);
 *
 *   // configuration
 *   pipeline->setXYZ(...); // optional
 *
 *   // rendering
 *   pipeline->render(myRenderFunc);
 *
 *
 */

class ForwardRenderingPipeline {


  private:

    glm::mat4 mView;
    glm::mat4 mProj;

    /// Render width in px
    int mWidth = 2;
    /// Render height in px
    int mHeight = 2;

    // === RENDER TARGETS ===
    SharedImage mImageColor;
    SharedImage mImageDepth;

    SharedImageView mViewColor;
    SharedImageView mViewDepth;

    // === Framebuffers ===
    SharedFramebuffer mFboForward;

    // === (Vulkan) RenderPasses ===
    SharedRenderPass mPassForward;
    SharedRenderPass mPassOutput;

    // === Pipelines ===
    SharedGraphicsPipeline mPipelineOutputFXAA;
    SharedGraphicsPipeline mPipelineOutputNoFXAA;

    // === Descriptor Sets ===
    SharedDescriptorSetLayout mOutputDescriptorLayout;
    SharedDescriptorSet mOutputDescriptor;

    // === View./Proj. Matrices ===
    SharedBuffer mViewProjBuffer;
    SharedDescriptorSetLayout mViewProjDescriptorSetLayout;
    SharedDescriptorSet mViewProjDescriptor;

    // === SETTINGS ===
    bool mFXAA = true;

    SharedDevice mDevice;
    vk::Format mOutputFormat;


  public: // getter, setter

    LAVA_GETTER(Width);
    LAVA_GETTER(Height);

    LAVA_PROPERTY(FXAA);


  public:

    ForwardRenderingPipeline (
            SharedDevice const &device,
            GenericFormat outputFormat
            );


    /// Resizes the internal pipeline size
    void
    resize (
            int w,
            int h
            );


    void
    updateViewProjMatrices (
            RecordingCommandBuffer &cmd,
            glm::mat4 view,
            glm::mat4 proj
            );


    /**
     * @brief executes the whole rendering pipeline
     * @param renderFunc function to call for every renderpass. Use the provided
     * pass information
     */
    void
    render (
            lava::RecordingCommandBuffer &cmd,
            lava::SharedFramebuffer const &fbo,
            const std::function<void(lava::pipeline::RenderPassSimple const &pass)> &renderFunc
            );


    /// Use this to create the FBOs for the Pipeline to output to
    SharedRenderPass const
    &outputPass() const
    {
        return mPassOutput;
    }


    /// Use this to create the Pipelines to render the forward pass
    SharedRenderPass const
    &forwardPass() const
    {
        return mPassForward;
    }


    Subpass
    opaquePass() const
    {
        return {mPassForward, 0};
    }


    SharedBuffer const
    &getViewProjBuffer() const
    {
        return mViewProjBuffer;
    }


    SharedDescriptorSet const
    &getViewProjDescriptor() const;


    SharedDescriptorSetLayout const&
    getViewProjDescriptorSetLayout() const;

};
}
}
