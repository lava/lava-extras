#pragma once

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

#include "fwd.hh"
#include <lava/fwd.hh>
#include <lava/createinfos/PipelineVertexInputStateCreateInfo.hh>

namespace lava {
class InlineSubpass;
class SecondarySubpass;

namespace geometry {

struct DefaultVertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 tangent;
    glm::vec2 texCoord;

    static void putAttributes(lava::PipelineVertexInputStateCreateInfo &info);
};


class Geometry;

class GeometryData {
  public:
    /// adds an attribute binding with the provided data.
    template <class T>
    void setBindingData(uint32_t binding, std::vector<T> const &data) {
        setBindingData(binding, reinterpret_cast<char const *>(data.data()),
                       sizeof(T) * data.size());
    }
    /// adds an attribute binding with the provided data. data is allowed to be
    /// NULL to signal placeholder data
    void setBindingData(uint32_t binding, char const *data, size_t size);

    void setIndices(std::vector<uint32_t> indices);

    /// upload all data into a buffer on the given Device
    std::shared_ptr<Geometry> uploadTo(SharedDevice const &device);

    lava::PipelineVertexInputStateCreateInfo &info() { return mSetup; }

  private:
    struct Binding {
        uint32_t binding;
        std::vector<char> data;
    };
    std::vector<uint32_t> mIndices;
    std::vector<Binding> mGeometryData;
    lava::PipelineVertexInputStateCreateInfo mSetup;
};

/**
 * This class roughly fullfills the purpose of a vertex array object in OpenGL.
 * It combines a Buffer of vertices and indices with the knowledge of how to
 * interpret those for rendering.
 */
class Geometry {
  public:
    template <class T>
    uint32_t updateBinding(uint32_t binding, std::vector<T> const &data) {
        return updateBinding(binding, data.data(), sizeof(T) * data.size());
    }
    /// Upload new data for a binding, size must be smaller or equal to the size
    /// given when the Geometry was uploaded
    void updateBinding(uint32_t binding, char const *data, size_t size);

    /// Upload new indices. The number of indices must be smaller or equal to
    /// the number given when the Geometry was uploaded
    void updateIndices(std::vector<uint32_t> const &indices);

    lava::PipelineVertexInputStateCreateInfo const &info() const {
        return mSetup;
    }

    void bind(InlineSubpass &pass);
    void bind(RecordingCommandBuffer &cmd);

    void draw(InlineSubpass &pass);
    void draw(RecordingCommandBuffer &cmd);

    void drawNoBind(InlineSubpass &pass);
    void drawNoBind(RecordingCommandBuffer &cmd);

  private:
    void bind(vk::CommandBuffer cmd);
    void draw(vk::CommandBuffer cmd);
    void drawNoBind(vk::CommandBuffer cmd);

    struct BufferRange {
        vk::DeviceSize offset;
        vk::DeviceSize size;
    };

    struct Binding {
        uint32_t binding;
        BufferRange range;
    };

    uint32_t mNumElements = 0;
    uint32_t mNumInstances = 1;

    BufferRange mIndices;
    vk::IndexType mIndexType;
    std::vector<Binding> mGeometryData;

    lava::SharedBuffer mBuffer;
    lava::PipelineVertexInputStateCreateInfo mSetup;

    friend class GeometryData;
};
}
}
