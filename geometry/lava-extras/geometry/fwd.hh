#pragma once
#include <memory>

#define LAVA_FORWARD_DECLARE_CLASS(T)                                          \
    class T;                                                                   \
    using Shared##T = std::shared_ptr<T>;                                      \
    using Weak##T = std::weak_ptr<T>;                                          \
    using Unique##T = std::unique_ptr<T>

namespace lava {
namespace geometry {

LAVA_FORWARD_DECLARE_CLASS(Geometry);
LAVA_FORWARD_DECLARE_CLASS(GeometryData);
} // namespace pipeline
} // namespace lava

#undef LAVA_FORWARD_DECLARE_CLASS
