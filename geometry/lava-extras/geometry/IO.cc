#include "IO.hh"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/vec4.hpp>
#include <fstream>
#include <lava/common/log.hh>

namespace lava {
namespace geometry {

static glm::vec3 aiCast(aiVector3D const &v) { return {v.x, v.y, v.z}; }
static glm::vec4 aiCast(aiColor4D const &v) { return {v.r, v.g, v.b, v.a}; }

std::vector<uint32_t> Importer::load(const std::string &filename,
                                     const Importer::VertexCB &cb) {

    if (!std::ifstream(filename).good()) {
        error() << "Error loading `" << filename << "'.";
        error() << "  File not found/not readable";
        return {};
    }

    uint32_t flags = aiProcess_SortByPType;

    if (mTriangulate)
        flags |= aiProcess_Triangulate;
    if (mCalculateTangents)
        flags |= aiProcess_CalcTangentSpace;
    if (mGenerateSmoothNormal)
        flags |= aiProcess_GenSmoothNormals;
    if (mGenerateFaceNormal)
        flags |= aiProcess_GenNormals;
    if (mGenerateUVCoords)
        flags |= aiProcess_GenUVCoords;
    if (mPreTransformVertices)
        flags |= aiProcess_PreTransformVertices;

    Assimp::Importer importer;
    auto scene = importer.ReadFile(filename, flags);

    if (!scene) {
        error() << "Error loading `" << filename << "' with Assimp.";
        error() << "  " << importer.GetErrorString();
        return {};
    }

    if (!scene->HasMeshes()) {
        error() << "File `" << filename << "' has no meshes.";
        return {};
    }

    std::vector<uint32_t> indices;

    auto baseIdx = 0u;
    for (auto i = 0u; i < scene->mNumMeshes; ++i) {
        auto const &mesh = scene->mMeshes[i];

        // add faces
        auto fCnt = mesh->mNumFaces;
        for (auto f = 0u; f < fCnt; ++f) {
            auto const &face = mesh->mFaces[f];
            if (face.mNumIndices != 3) {
                error() << "File `" << filename << "':.";
                error() << "  non-3 faces not implemented/supported "
                        << face.mNumIndices;
                continue;
            }

            for (auto fi = 0u; fi < face.mNumIndices; ++fi) {
                indices.push_back(baseIdx + face.mIndices[fi]);
            }
        }

        // add vertices
        auto vCnt = mesh->mNumVertices;
        for (auto v = 0u; v < vCnt; ++v) {
            auto pos = aiCast(mesh->mVertices[v]);
            auto normal = mesh->HasNormals() ? aiCast(mesh->mNormals[v])
                                             : glm::vec3(0.0f);
            auto tangent = mesh->HasTangentsAndBitangents()
                               ? aiCast(mesh->mTangents[v])
                               : glm::vec3(0.0f);
            auto texCoord = mesh->HasTextureCoords(0)
                                ? aiCast(mesh->mTextureCoords[0][v])
                                : glm::vec3(0.f);
            auto colors = mesh->HasVertexColors(0) ? aiCast(mesh->mColors[0][v])
                                                   : glm::vec4(1.f);
            cb(pos, normal, tangent, texCoord, colors);
        }

        baseIdx += mesh->mNumVertices;
    }

    return indices;
}

void DefaultVertex::putAttributes(PipelineVertexInputStateCreateInfo &info) {
    info.binding(0,                     //
                 &DefaultVertex::position, //
                 &DefaultVertex::normal,   //
                 &DefaultVertex::tangent,  //
                 &DefaultVertex::texCoord);
}

lava::geometry::Importer::Importer() {}
}
}
