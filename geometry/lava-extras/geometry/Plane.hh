#pragma once

#include <lava/fwd.hh>
#include "Geometry.hh"

namespace lava {

namespace geometry {

template <class VertexT = DefaultVertex> class Plane : public GeometryData {
  public:
    Plane(float _width = 1, float _height = 1)
        : width(_width), height(_height) {
        generate();
    }
    static std::shared_ptr<Plane<VertexT>> create(float _width = 1,
                                                  float _height = 1) {
        return std::make_shared<Plane<VertexT>>(_width, _height);
    }

    float width;
    float height;

    void generate() {
        std::vector<VertexT> vertices(4);
        auto corner = glm::vec3(width, height, 0);
        auto normal = glm::vec3(0, 1, 0);
        auto tangent = glm::vec3(1, 0, 0);
        vertices[0] = {
            corner * glm::vec3(1.f, 1.f, 1.f), normal, tangent, {1.f, 1.f}};
        vertices[1] = {
            corner * glm::vec3(1.f, -1.f, 1.f), normal, tangent, {1.f, -1.f}};
        vertices[2] = {
            corner * glm::vec3(-1.f, -1.f, 1.f), normal, tangent, {-1.f, -1.f}};
        vertices[3] = {
            corner * glm::vec3(-1.f, 1.f, 1.f), normal, tangent, {-1.f, 1.f}};

        std::vector<uint32_t> indices = {0,1,3,1,2,3};


        setBindingData(0, vertices);
        setIndices(indices);

        VertexT::putAttributes(info());
    }
};

} // namespace geometry
} // namespace lava
