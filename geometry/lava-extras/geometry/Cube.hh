#pragma once

#include <glm/glm.hpp>

#include <lava/fwd.hh>
#include "Geometry.hh"

namespace lava {

namespace geometry {

template <class VertexT = DefaultVertex> class Cube : public GeometryData {
  public:
    Cube(glm::vec3 _minCoord = {-1, -1, -1}, glm::vec3 _maxCoord = {+1, +1, +1})
        : minCoord(_minCoord), maxCoord(_maxCoord) {
        generate();
    }
    static std::shared_ptr<Cube<VertexT>>
    create(glm::vec3 _minCoord = {-1, -1, -1},
           glm::vec3 _maxCoord = {+1, +1, +1}) {
        return std::make_shared<Cube<VertexT>>(_minCoord, _maxCoord);
    }

    glm::vec3 minCoord, maxCoord;

    void generate() {

        auto vi = 0u;
        auto ii = 0u;

        std::vector<VertexT> vertices(4 * 3 * 2);
        std::vector<uint32_t> indices(6 * 3 * 2, 0);

        for (int i = 0; i < 3; ++i) {
            for (int s = 0; s < 2; ++s) {
                glm::vec3 n{i == 0, i == 1, i == 2};
                n *= s * 2 - 1;

                auto mid = n;
                auto left = glm::abs(dot(n, glm::vec3(0, 1, 0))) < .2
                                ? cross(glm::vec3(0, 1, 0), n)
                                : cross(glm::vec3(0, 0, 1), n);
                auto top = cross(left, n);

                auto baseIndex = vi;

                vertices[vi++] = {
                    mix(minCoord, maxCoord, (mid - left - top) * .5f + .5f),
                    n,
                    left,
                    {0, 0}};
                vertices[vi++] = {
                    mix(minCoord, maxCoord, (mid - left + top) * .5f + .5f),
                    n,
                    left,
                    {0, 1}};
                vertices[vi++] = {
                    mix(minCoord, maxCoord, (mid + left - top) * .5f + .5f),
                    n,
                    left,
                    {1, 0}};
                vertices[vi++] = {
                    mix(minCoord, maxCoord, (mid + left + top) * .5f + .5f),
                    n,
                    left,
                    {1, 1}};

                indices[ii++] = baseIndex + 0;
                indices[ii++] = baseIndex + 1;
                indices[ii++] = baseIndex + 2;

                indices[ii++] = baseIndex + 1;
                indices[ii++] = baseIndex + 3;
                indices[ii++] = baseIndex + 2;
            }
        }

        setBindingData(0, vertices);
        setIndices(indices);

        VertexT::putAttributes(info());
    }
};

} // namespace geometry
} // namespace lava
