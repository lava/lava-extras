#pragma once
#include "fwd.hh"
#include "Geometry.hh"
#include <functional>
#include <glm/vec4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <vector>
#include <lava/common/property.hh>

namespace lava {
namespace geometry {

class Importer {

  private: // settings
    // see
    // http://www.assimp.org/lib_html/postprocess_8h.html#a64795260b95f5a4b3f3dc1be4f52e410a8857a0e30688127a82c7b8939958c6dc
    /// vec3 aTangent is available
    bool mCalculateTangents = true;
    /// No quads.
    bool mTriangulate = true;
    /// vec3 aNormal is available
    /// (normals in the dataset have priority)
    bool mGenerateSmoothNormal = true;
    /// per face, non-smooth normals
    bool mGenerateFaceNormal = false;

    /// Collapses scene structures
    bool mPreTransformVertices = true;
    /// vec2 aTexCoord is available (hopefully)
    bool mGenerateUVCoords = true;

  public:
    LAVA_PROPERTY(CalculateTangents);
    LAVA_PROPERTY(Triangulate);
    LAVA_PROPERTY(GenerateSmoothNormal);
    LAVA_PROPERTY(GenerateFaceNormal);
    LAVA_PROPERTY(PreTransformVertices);
    LAVA_PROPERTY(GenerateUVCoords);

    Importer();

    using VertexCB =
        std::function<void(glm::vec3 /* position */, glm::vec3 /* normal */,
                           glm::vec3 /* tangent */, glm::vec3 /* texcoord */,
                           glm::vec4 /* color */)>;

    /// Loads the given mesh and calls VertexCB for every vertex. You can use
    /// this if you want more control over the layout of the data.
    std::vector<uint32_t> load(std::string const &filename, VertexCB const &cb);

    template <class VertexT = DefaultVertex>
    GeometryData load(std::string const &filename) {
        std::vector<VertexT> vertices;
        auto indices = load(filename, [&](glm::vec3 pos, glm::vec3 normal,
                                           glm::vec3 tangent,
                                           glm::vec3 texcoord, glm::vec4) {
            vertices.push_back({
                pos, normal, tangent, glm::vec2(texcoord),
            });
        });

        GeometryData result;
        VertexT::putAttributes(result.info());
        result.setIndices(indices);
        result.setBindingData(0, vertices);

        return result;
    }
};
}
}
