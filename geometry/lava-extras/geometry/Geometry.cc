#include "Geometry.hh"
#include <lava/objects/Device.hh>
#include <lava/objects/Buffer.hh>
#include <lava/createinfos/Buffers.hh>
#include <lava/raii/ActiveRenderPass.hh>
#include <lava/common/log.hh>

namespace lava {
namespace geometry {

void GeometryData::setBindingData(uint32_t binding, const char *data,
                                  size_t size) {
    if (data) {
        mGeometryData.push_back({binding, {data, data + size}});
    } else {
        mGeometryData.push_back({binding, std::vector<char>(size, '\0')});
    }
}

void GeometryData::setIndices(std::vector<uint32_t> indices) {
    mIndices = indices;
}

std::shared_ptr<Geometry> lava::geometry::GeometryData::uploadTo(const SharedDevice &device) {
    std::vector<char> combined;
    auto setup = mSetup.get();

    auto result = std::make_shared<Geometry>();
    result->mSetup = mSetup;

    if (!mIndices.empty()) {
        auto max_index = *max_element(begin(mIndices), end(mIndices));
        if (max_index <= std::numeric_limits<uint16_t>::max()) {
            std::vector<uint16_t> shortIndices = {begin(mIndices),
                                                  end(mIndices)};
            combined.resize(shortIndices.size() * sizeof(uint16_t));
            auto ptr = reinterpret_cast<char const *>(shortIndices.data());
            std::copy(ptr, ptr + combined.size(), combined.data());
            result->mNumElements = mIndices.size();
            result->mIndexType = vk::IndexType::eUint16;
        } else {
            combined.resize(mIndices.size() * sizeof(uint32_t));
            auto ptr = reinterpret_cast<char const *>(mIndices.data());
            std::copy(ptr, ptr + combined.size(), combined.data());
            result->mNumElements = mIndices.size();
            result->mIndexType = vk::IndexType::eUint32;
        }

        result->mIndices = {0, combined.size()};
    }

    for (auto binding : mGeometryData) {
        auto offset = combined.size();

        if (offset % 4 != 0) {
            offset += 4 - (offset % 4);
        }

        auto &data = binding.data;
        combined.resize(offset + data.size());
        copy(begin(data), end(data), combined.data() + offset);

        result->mGeometryData.push_back(
            {binding.binding, {offset, data.size()}});

        assert(binding.binding < setup->vertexBindingDescriptionCount);
        auto bindingDesc = setup->pVertexBindingDescriptions;

        assert(data.size() % bindingDesc->stride == 0);
        if (bindingDesc->inputRate == vk::VertexInputRate::eInstance) {
            auto num_instances = uint32_t(data.size() / bindingDesc->stride);
            assert(((result->mNumInstances == 1) ||
                    (num_instances == result->mNumElements)) &&
                   "The provided sets of binding data appear to disagree on "
                   "the number of instances.");
            result->mNumInstances = num_instances;
        } else if (mIndices.empty()) {
            auto num_elements = uint32_t(data.size() / bindingDesc->stride);
            assert((!result->mNumElements ||
                    (num_elements == result->mNumElements)) &&
                   "The provided sets of binding data appear to disagree on "
                   "the number of elements.");
            result->mNumElements = num_elements;
        }
    }

    result->mBuffer = device->createBuffer(indexArrayBuffer());
    result->mBuffer->setDataVRAM(combined);

    return result;
}

void Geometry::bind(InlineSubpass &pass)
{
    bind(pass.getCommandBuffer());
}

void Geometry::bind(RecordingCommandBuffer &cmd)
{
    bind(cmd.buffer()->handle());
}

void Geometry::draw(InlineSubpass &pass) { draw(pass.getCommandBuffer()); }

void Geometry::draw(RecordingCommandBuffer &cmd) {
    draw(cmd.buffer()->handle());
}

void Geometry::drawNoBind(InlineSubpass &pass) {
    drawNoBind(pass.getCommandBuffer());
}

void Geometry::drawNoBind(RecordingCommandBuffer &cmd) {
    drawNoBind(cmd.buffer()->handle());
}

void Geometry::bind(vk::CommandBuffer cmd) {
    if (mIndices.size)
        cmd.bindIndexBuffer(mBuffer->handle(), mIndices.offset, mIndexType);

    std::vector<vk::Buffer> buffers(mGeometryData.size(), mBuffer->handle());
    std::vector<vk::DeviceSize> offsets(mGeometryData.size(), 0u);
    for (auto &b : mGeometryData)
        offsets[b.binding] = b.range.offset;

    cmd.bindVertexBuffers(0, buffers, offsets);
}

void Geometry::draw(vk::CommandBuffer cmd) {
    bind(cmd);
    drawNoBind(cmd);
}

void Geometry::drawNoBind(vk::CommandBuffer cmd) {
    if (mIndices.size) {
        cmd.drawIndexed(mNumElements, mNumInstances, 0, 0, 0);
    } else {
        cmd.draw(mNumElements, mNumInstances, 0, 0);
    }
}
}
}
