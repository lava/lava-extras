#pragma once
#include <imgui/imgui.h>
#include <lava/common/GlLikeFormats.hh>
#include <lava/fwd.hh>
#include <vector>

namespace lava {
namespace imgui {

struct ImGuiFrame {
    ImGuiFrame() : active(true){};
    ImGuiFrame(ImGuiFrame const &) = delete;
    ImGuiFrame(ImGuiFrame &&o) { o.active = false; }
    ~ImGuiFrame();

    bool active;
};

class ImGui {
  public:
    ImGui(SharedDevice const &device,
          vk::ImageLayout finalLayout = vk::ImageLayout::ePresentSrcKHR);

    /// You need to either connect a window.... (the imgui will be rendered
    /// ontop of whatever is on the window already, when render() is called)
    void connectWindow(lava::features::SharedGlfwWindow const &window);
    void prepare(std::vector<SharedImageView> const &views);

    /// ...or connect ImageView(s) (all of which must have the same size), mind
    /// that you need to use ImGuiIO to pipe in controls still.
    /// (images will be cleared before rendering by default)
    void connectViews(std::vector<SharedImageView> const &views, bool clear = true);

    void render(int viewindex, RecordingCommandBuffer &cmd);

    ImGuiFrame frame();

    /// select this GUIs context as current context (i.e. to use IO);
    void makeCurrent();

  protected:
    vk::ImageLayout mFinalLayout;
    void setupPass(bool clear, vk::Format format);

    lava::SharedDescriptorPool mDescriptorPool;
    lava::SharedRenderPass mRenderPass;
    std::vector<SharedFramebuffer> mFbos;

    enum Usage { UNDEFINED, TO_GLFW_WINDOW, TO_TEXTURE };
    Usage mUsage = UNDEFINED;

    ImGuiContext *mContext;
};

} // namespace imgui
} // namespace lava
