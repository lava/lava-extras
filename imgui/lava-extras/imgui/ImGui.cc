#include "ImGui.hh"

#include <lava/features/GlfwWindow.hh>
#include <lava/objects/DescriptorPool.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Framebuffer.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/ImageView.hh>
#include <lava/objects/Instance.hh>
#include <lava/objects/RenderPass.hh>
#include <lava/raii/ActiveRenderPass.hh>

#include <imgui/imgui.h>
#include "impl/imgui_impl_glfw.hh"
#include "impl/imgui_impl_lava.hh"

namespace lava {
namespace imgui {

ImGui::ImGui(const SharedDevice &device, vk::ImageLayout finalLayout) {
    mFinalLayout = finalLayout;
    mDescriptorPool =
        DescriptorPoolCreateInfo()
            .allowFreeing()
            .setMaxSets(10)
            .addSize(vk::DescriptorType::eCombinedImageSampler, 10)
            .create(device);

    mContext = ::ImGui::CreateContext();
    makeCurrent();
}

void ImGui::connectWindow(const features::SharedGlfwWindow &window) {
    ::ImGui::SetCurrentContext(mContext);
    mUsage = TO_GLFW_WINDOW;
    setupPass(false, window->format());
    ImGui_ImplGlfw_Lava_InitForVulkan(window->window(), true);
}

void ImGui::prepare(const std::vector<SharedImageView> &views) {
    mFbos.clear();
    for (auto const &view : views) {
        std::vector<SharedImageView> view_vec = {view};
        mFbos.push_back(std::make_shared<Framebuffer>(mRenderPass, view_vec));
    }
}

void ImGui::connectViews(const std::vector<SharedImageView> &views, bool clear) {
    assert(!views.empty());
    ::ImGui::SetCurrentContext(mContext);

    mUsage = TO_TEXTURE;
    setupPass(clear, views[0]->format());
    prepare(views);

    ImGuiIO &io = ::ImGui::GetIO();
    io.DisplaySize.x = float(views[0]->image()->width());
    io.DisplaySize.y = float(views[0]->image()->height());

    io.BackendPlatformName = "Lava VR Imgui";
    io.BackendRendererName = "Lava Renderer";

    io.GetClipboardTextFn = nullptr;
    io.SetClipboardTextFn = nullptr;
    io.ClipboardUserData = nullptr;

    io.ImeSetInputScreenPosFn = nullptr;
    io.ImeWindowHandle = nullptr;
}

void ImGui::render(int viewindex, RecordingCommandBuffer &cmd) {
    ::ImGui::SetCurrentContext(mContext);
    auto dd = ::ImGui::GetDrawData();
    if (dd->Valid) {
        auto pass = cmd.beginRenderpass(mFbos[viewindex]);
        {
            auto sub = pass.startInlineSubpass();
            ImGui_ImplLava_RenderDrawData(::ImGui::GetDrawData(),
                                            cmd.buffer()->handle());
            (void)sub;
        }
    }
}

ImGuiFrame ImGui::frame() {
    makeCurrent();
    ImGui_ImplLava_NewFrame();

    assert(mUsage != UNDEFINED &&
           "You need to either connect a window or ImageView(s) before you can "
           "start recording the GUI");

    if (mUsage == TO_GLFW_WINDOW)
        ImGui_ImplGlfw_Lava_NewFrame();

    ::ImGui::NewFrame();
    return ImGuiFrame();
}

void ImGui::makeCurrent()
{
    ::ImGui::SetCurrentContext(mContext);
}


void ImGui::setupPass(bool clear, vk::Format format) {
    if (mRenderPass)
        return;

    auto const &device = mDescriptorPool->device();

    auto attachment =
        AttachmentDescription::color(format).finalLayout(mFinalLayout);
    if (clear)
        attachment.clear();

    mRenderPass = RenderPassCreateInfo()
                      .addAttachment(attachment)
                      .addSubpass(SubpassDescription().colors({0}))
                      .create(device);

    ImGui_ImplVulkan_InitInfo info;
    info.Queue = device->graphicsQueue().handle();
    info.QueueFamily = device->graphicsQueue().family();
    info.Device = device->handle();
    info.Instance = device->instance()->handle();
    info.Allocator = nullptr;
    info.PipelineCache = VK_NULL_HANDLE;
    info.DescriptorPool = mDescriptorPool->handle();
    info.PhysicalDevice = device->physical();
    info.CheckVkResultFn = nullptr;

    ImGui_ImplLava_Init(&info, mRenderPass->handle());

    auto cmd = device->graphicsQueue().beginCommandBuffer();
    ImGui_ImplLava_CreateFontsTexture(cmd.buffer()->handle());
}

ImGuiFrame::~ImGuiFrame() {
    if (active)
        ::ImGui::Render();
}

} // namespace imgui
} // namespace lava
