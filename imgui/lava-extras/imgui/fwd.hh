#pragma once
#include <memory>

namespace lava {
namespace imgui {

#define LAVA_FORWARD_DECLARE_CLASS(T)                                          \
    class T;                                                                   \
    using Shared##T = std::shared_ptr<T>;                                      \
    using Weak##T = std::weak_ptr<T>;                                          \
    using Unique##T = std::unique_ptr<T>
LAVA_FORWARD_DECLARE_CLASS(ImGui);
struct ImGuiFrame;
} // namespace imgui
} // namespace lava

#undef LAVA_FORWARD_DECLARE_CLASS
