#include "GlfwApp.hh"
#include <lava/objects/Instance.hh>
#include <lava/objects/Device.hh>
#include <lava/features/GlfwWindow.hh>
#include <lava/features/GlfwOutput.hh>
#include <lava/features/Validation.hh>
#include <lava/gpuselection/NthOfTypeStrategy.hh>
#include <lava/objects/Queue.hh>
#include <lava-extras/pipeline/RenderingPipeline.hh>
#include <lava/objects/RenderPass.hh>
#include <GLFW/glfw3.h>
#include <lava/common/log.hh>
#include <fmt/format.hh>
#include <lava-extras/camera/GenericCamera.hh>
#include <thread>

namespace lava {
namespace glfw {
GlfwApp::~GlfwApp() {}

static std::string thousandSep(size_t val) {
    auto s = std::to_string(val);
    auto l = s.size();
    while (l > 3) {
        s = s.substr(0, l - 3) + "'" + s.substr(l - 3);
        l -= 3;
    }
    return s;
}

void GlfwApp::mainLoop() {
    // Loop until the user closes the window
    int frames = 0;
    double lastTime = glfwGetTime();
    double lastStatsTime = lastTime;
    double timeAccum = 0.000001;
    double renderTimestep = 1.0 / mUpdateRate;
    mCurrentTime = 0.0;
    size_t primitives = 0;
    size_t fragments = 0;
    double gpuTime = 0;
    double cpuTime = 0;
    while (!shouldClose()) {
        updateInput();

        // Update
        auto maxTicks = mMaxFrameSkip;
        while (timeAccum > 0) {
            if (maxTicks-- < 0) {
                lava::warning() << "Too many updates queued, frame skip of "
                                << timeAccum << " secs";
                timeAccum = 0.0;
                break;
            }

            auto dt = 1.0 / mUpdateRate;
            auto cpuStart = glfwGetTime();
            update((float)dt);
            cpuTime += glfwGetTime() - cpuStart;
            timeAccum -= dt;
            mCurrentTime += dt;
        }

        // Render here
        {
            // if (mQueryStats && mPrimitiveQuery)
            //{
            //    // assert(mPrimitiveQuery && "did you forgot to call
            //    GlfwApp::init() in your init?");
            //    mPrimitiveQuery->begin();
            //    mOcclusionQuery->begin();
            //    mRenderStartQuery->saveTimestamp();
            //}

            render((float)renderTimestep);

            // if (mQueryStats && mPrimitiveQuery)
            //{
            //    mPrimitiveQuery->end();
            //    mOcclusionQuery->end();
            //    mRenderEndQuery->saveTimestamp();
            //}
        }

        // timing
        auto now = glfwGetTime();
        renderTimestep = now - lastTime;
        timeAccum += now - lastTime;
        lastTime = now;
        ++frames;

        // if (mQueryStats && mPrimitiveQuery)
        //{
        //    primitives += mPrimitiveQuery->getResult64();
        //    fragments += mOcclusionQuery->getResult64();
        //    gpuTime +=
        //    TimerQuery::toSeconds(mRenderEndQuery->getSavedTimestamp() -
        //    mRenderStartQuery->getSavedTimestamp());
        //}

        if (mOutputStatsInterval > 0 &&
            lastTime > lastStatsTime + mOutputStatsInterval) {
            double fps = frames / (lastTime - lastStatsTime);
            if (mQueryStats)
                lava::info() << fmt::format(
                    "FPS: {:.1f}, CPU: {:.1f} ms, GPU: {:.1f} ms, Primitives: "
                    "{}, Frags: {}",
                    fps, cpuTime / frames * 1000., gpuTime / frames * 1000.,
                    thousandSep(primitives / frames),
                    thousandSep(fragments / frames));
            else
                lava::info() << fmt::format("FPS: {:.1f}, CPU: {:.1f} ms", fps,
                                            cpuTime / frames * 1000.);
            lastStatsTime = lastTime;
            frames = 0;
            primitives = 0;
            fragments = 0;
            gpuTime = 0;
            cpuTime = 0;
        }
    }
}

void GlfwApp::setTitle(const std::string &title) {
    mTitle = title;
    if (mWindow)
        glfwSetWindowTitle(mWindow->window(), title.c_str());
}

void GlfwApp::setClipboardString(const std::string &s) const {
    glfwSetClipboardString(mWindow->window(), s.c_str());
}

std::string GlfwApp::getClipboardString() const {
    auto s = glfwGetClipboardString(mWindow->window());
    return s ? s : "";
}

bool GlfwApp::isMouseButtonPressed(int button) const {
    return glfwGetMouseButton(mWindow->window(), button) == GLFW_PRESS;
}

bool GlfwApp::isKeyPressed(int key) const {
    return glfwGetKey(mWindow->window(), key) == GLFW_PRESS;
}

bool GlfwApp::shouldClose() const {
    return glfwWindowShouldClose(mWindow->window());
}

void GlfwApp::init() {
    if (mUseDefaultRendering) {
        // create camera with some sensible defaults
        mCamera = std::make_shared<camera::GenericCamera>();
        mCamera->setVerticalFieldOfView(60);
        mCamera->setPosition({5, 2, 5});
        mCamera->setTarget({0, 0, 0});

        // set up rendering pipeline
        mPipeline = std::make_shared<pipeline::RenderingPipeline>(
            mDevice, mGlfw->format());
        mPipeline->setCamera(mCamera);

        // mDebugRenderer = std::make_shared<debugging::DebugRenderer>();
    }

    // mPrimitiveQuery = std::make_shared<PrimitiveQuery>();
    // mOcclusionQuery = std::make_shared<OcclusionQuery>();
    // mRenderStartQuery = std::make_shared<TimerQuery>();
    // mRenderEndQuery = std::make_shared<TimerQuery>();
}

void GlfwApp::update(float elapsedSeconds) {
    auto window = mWindow->window();
    if (mCamera && mUseDefaultCameraHandling) {
        auto speed = mCameraMoveSpeed;
        if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
            speed *= mCameraMoveSpeedFactor;

        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
            mCamera->moveForward(elapsedSeconds * speed);
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
            mCamera->moveBack(elapsedSeconds * speed);
        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
            mCamera->moveLeft(elapsedSeconds * speed);
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
            mCamera->moveRight(elapsedSeconds * speed);
    }
}

void GlfwApp::render(float elapsedSeconds) {
    mDevice->graphicsQueue().catchUp(1);

    auto frame = mWindow->startFrame();

    auto const &fbo = mSwapchainFbos[frame.imageIndex()];

    auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
    cmd.wait(frame.imageReady());
    cmd.signal(frame.renderingComplete());

    if (mUseDefaultRendering) {
        assert(mPipeline &&
               "did you forgot to call GlfwApp::init() in your init?");
        mPipeline->render(
            cmd, fbo, [this, elapsedSeconds](const pipeline::RenderPass &pass) {
                renderPass(pass, elapsedSeconds);
            });
    }
}

void GlfwApp::onResize(int w, int h) {
    if (mUseDefaultRendering) {
        if (mCamera)
            mCamera->resize(w, h);
        if (mPipeline)
            mPipeline->resize(w, h);
    }
}

void GlfwApp::renderPass(const pipeline::RenderPass &pass,
                         float elapsedSeconds) {
    if (mUseDefaultRendering) {
        // debug()->renderPass(pass);
    }
}

void GlfwApp::onClose() {}

bool GlfwApp::onKey(int key, int scancode, int action, int mods) {
    // if (TwEventKeyGLFW(mWindow, key, scancode, action, mods))
    //    return true;

    if (key == GLFW_KEY_HOME && action == GLFW_PRESS) {
        onResetView();
        return true;
    }

    if (key == GLFW_KEY_F && action == GLFW_PRESS && mPipeline) {
        mPipeline->setFXAA(!mPipeline->getFXAA());
        lava::info() << "Turned FXAA "
                     << ((mPipeline->getFXAA()) ? "on" : "off");
    }

    return false;
}

bool GlfwApp::onMousePosition(double x, double y) {
    // if (TwEventMousePosGLFW(mWindow, x, y))
    //    return true;

    auto window = mWindow->window();
    if (mMouseLastX >= 0.0 && mCamera && mUseDefaultCameraHandling) {
        auto shift = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS;
        auto alt = glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS;
        auto ctrl = glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS;

        auto dx = x - mMouseLastX;
        auto dy = y - mMouseLastY;

        float ax = (float)(dx / mWindowWidth * mCameraTurnSpeed);
        float ay = (float)(dy / mWindowHeight * mCameraTurnSpeed);

        if (mMouseRight && !alt && !ctrl) // from cam
        {
            mCamera->FPSstyleLookAround(ax, ay);
        }
        if (mMouseLeft && !shift && !alt && !ctrl) // around target
        {
            auto fwd = mCamera->getForwardDirection();

            auto azimuth = glm::atan(fwd.z, fwd.x) + ax;
            auto altitude =
                glm::atan(fwd.y, glm::sqrt(fwd.x * fwd.x + fwd.z * fwd.z)) - ay;
            altitude = glm::clamp(altitude, -0.499f * glm::pi<float>(),
                                  0.499f * glm::pi<float>());

            auto caz = glm::cos(azimuth);
            auto saz = glm::sin(azimuth);
            auto cal = glm::cos(altitude);
            auto sal = glm::sin(altitude);

            auto newFwd = glm::vec3(cal * caz,  // x
                                    sal,        // y
                                    cal * saz); // z

            auto target = mCamera->getTarget();

            auto dis = mCamera->getLookAtDistance();
            mCamera->setPosition(target - newFwd * dis);
            mCamera->setTarget(target, {0, 1, 0});
        }
    }

    mMouseLastX = x;
    mMouseLastY = y;

    return false;
}

bool GlfwApp::onMouseButton(double x, double y, int button, int action,
                            int mods, int clickCount) {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
        mMouseLeft = false;
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
        mMouseRight = false;

    // if (TwEventMouseButtonGLFW(mWindow, button, action, mods))
    //    return true;

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
        mMouseLeft = true;
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
        mMouseRight = true;

    // Double [MMB] (no mods) -> reset view
    if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS &&
        clickCount > 1 && mods == 0)
        onResetView();

    // Double [LMB] (no mods) -> center on pos
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS &&
        clickCount > 1 && mods == 0) {
        glm::vec3 pos;
        float depth;
        if (mUseDefaultRendering && mUseDefaultCameraHandling &&
            mPipeline->queryPosition3D((int)x, (int)y, &pos, &depth)) {
            mCamera->setPosition(pos -
                                 mCamera->getForwardDirection() *
                                     mCamera->getLookAtDistance());
            mCamera->setTarget(pos, {0, 1, 0});
        }
    }

    return false;
}

bool GlfwApp::onMouseScroll(double sx, double sy) {
    // if (TwEventMouseWheelGLFW(mWindow, sx, sy))
    //    return true;

    // camera handling
    if (mCamera && mUseDefaultCameraHandling && sy != 0) {
        auto f = glm::pow(1 + mCameraScrollSpeed / 100.0, -sy);
        float camDis = mCamera->getLookAtDistance() * (float)f;
        camDis = glm::clamp(camDis, mCamera->getNearClippingPlane() * 2,
                            mCamera->getFarClippingPlane() / 2);

        // update target AND lookAtDis
        mCamera->setPosition(mCamera->getTarget() -
                             mCamera->getForwardDirection() * camDis);
        mCamera->setLookAtDistance(camDis);
    }

    return false;
}

bool GlfwApp::onMouseEnter() { return false; }

bool GlfwApp::onMouseExit() { return false; }

bool GlfwApp::onFocusGain() { return false; }

bool GlfwApp::onFocusLost() { return false; }

bool GlfwApp::onFileDrop(const std::vector<std::string> &files) {
    return false;
}

void GlfwApp::onResetView() {
    mCamera->setPosition(-mCamera->getForwardDirection() *
                         mCamera->getLookAtDistance());
    mCamera->setTarget({0, 0, 0}, {0, 1, 0});
}

bool GlfwApp::onChar(unsigned int codepoint, int mods) {
    // if (TwEventCharGLFW(mWindow, codepoint))
    //    return true;

    return false;
}

int GlfwApp::run(int argc, char *argv[]) {
    mGlfw = features::GlfwOutput::create();
    auto instance =
        lava::Instance::create({lava::features::Validation::create(), mGlfw});

    mDevice = instance->createDevice(
        {lava::QueueRequest::graphics("graphics")},
        lava::NthOfTypeStrategy(vk::PhysicalDeviceType::eDiscreteGpu));

    init();

    mWindow = mGlfw->openWindow();
    setupGlfwCallbacks();
    mWindow->buildSwapchainWith(
        [&](std::vector<lava::SharedImageView> const &views) {
            mSwapchainFbos.clear();
            for (auto &view : views)
                mSwapchainFbos.push_back(
                    mPipeline->outputPass()->createFramebuffer({view}));
            onResize(int(mWindow->width()), int(mWindow->height()));
        });

    mainLoop();

    onClose();

    return 0;
}

void GlfwApp::internalOnMouseButton(double x, double y, int button, int action,
                                    int mods) {
    // check double click
    if (distance(mClickPos, glm::vec2(x, y)) > 5) // too far
        mClickCount = 0;
    if (mClickTimer.seconds() > mDoubleClickTime) // too slow
        mClickCount = 0;
    if (mClickButton != button) // wrong button
        mClickCount = 0;

    mClickTimer.reset();
    mClickButton = button;
    mClickPos = {x, y};
    mClickCount++;

    onMouseButton(x, y, button, action, mods, mClickCount);
}

void GlfwApp::setupGlfwCallbacks() {
    static std::unordered_map<GLFWwindow *, GlfwApp *> sWindowApps;

    auto window = mWindow->window();
    auto insertion = sWindowApps.insert({window, this}).second;
    assert(insertion && "You can only have one app per GLFWwindow.");

    glfwSetKeyCallback(window, [](GLFWwindow *win, int key, int scancode,
                                  int action, int mods) {
        sWindowApps.at(win)->onKey(key, scancode, action, mods);
    });

    glfwSetCharModsCallback(
        window, [](GLFWwindow *win, unsigned int codepoint,
                   int mods) { sWindowApps.at(win)->onChar(codepoint, mods); });

    glfwSetMouseButtonCallback(
        window, [](GLFWwindow *win, int button, int action, int mods) {
            auto app = sWindowApps.at(win);
            app->internalOnMouseButton(app->mMouseX, app->mMouseY, button,
                                       action, mods);
        });

    glfwSetCursorEnterCallback(window, [](GLFWwindow *win, int entered) {
        try {
            auto app = sWindowApps.at(win);
            if (entered)
                app->onMouseEnter();
            else
                app->onMouseExit();
        } catch (std::out_of_range) {
        }
    });

    glfwSetCursorPosCallback(window, [](GLFWwindow *win, double x, double y) {
        auto app = sWindowApps.at(win);
        app->mMouseX = x;
        app->mMouseY = y;
        app->onMousePosition(x, y);
    });

    glfwSetScrollCallback(window, [](GLFWwindow *win, double sx, double sy) {
        sWindowApps.at(win)->onMouseScroll(sx, sy);
    });

    glfwSetFramebufferSizeCallback(window, [](GLFWwindow *win, int w, int h) {
        auto app = sWindowApps.at(win);
        app->mWindowWidth = w;
        app->mWindowHeight = h;
        app->onResize(w, h);
        // TwWindowSize(w, h);
    });

    glfwSetWindowFocusCallback(window, [](GLFWwindow *win, int focused) {
        auto app = sWindowApps.at(win);
        if (focused)
            app->onFocusGain();
        else
            app->onFocusLost();
    });

    glfwSetDropCallback(window,
                        [](GLFWwindow *win, int count, const char **paths) {
        std::vector<std::string> files;
        for (auto i = 0; i < count; ++i)
            files.push_back(paths[i]);
        sWindowApps.at(win)->onFileDrop(files);
    });
}

void GlfwApp::updateInput() {
    // update cursor mode
    switch (mCursorMode) {
    case CursorMode::Normal:
        glfwSetInputMode(mWindow->window(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        break;
    case CursorMode::Hidden:
        glfwSetInputMode(mWindow->window(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
        break;
    case CursorMode::Disabled:
        glfwSetInputMode(mWindow->window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        break;
    }

    // Poll for and process events
    glfwPollEvents();
}

void GlfwApp::sleepSeconds(double seconds) const {
    if (seconds <= 0.0)
        return;

    std::this_thread::sleep_for(std::chrono::duration<double>(seconds));
}
}
}
