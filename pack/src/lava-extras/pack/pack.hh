#include <string>
#include <lava/common/vulkan.hh>
#include "Package.hh"
#include <lava/fwd.hh>

namespace lava {
namespace pack {

Package get(std::string const &filename);
std::string string(std::string const &filename);

void debug();

/// Create a shader module from a shader that was packed into the binary
SharedShaderModule shader(SharedDevice const &device, const std::string &name);
}
}
