#include "pack.hh"
#include "Package.hh"

#include <lava/objects/Device.hh>
#include <string>
#include <unordered_map>
#include <lava/common/log.hh>
#include <cassert>
#include <lava/common/ShaderExtensions.hh>
#include <lava/objects/ShaderModule.hh>

namespace lava {
namespace pack {

namespace internal {
extern void putPackages(std::unordered_map<std::string, Package> &map);
}

namespace {
std::unordered_map<std::string, Package> sPackages = []() {
    std::unordered_map<std::string, Package> result;
    internal::putPackages(result);
    return result;
}();
}

Package get(const std::string &name) {
    try {
        return sPackages.at(name);
    } catch (std::out_of_range e) {
        lava::error() << name << " is not a packed file.";
        throw e;
    }
};

void debug() {
    lava::info() << "[PackageRegistry] current entries:";
    for (auto const &pair : sPackages) {
        lava::info() << " - " << pair.first << " number of bytes:"
                     << std::distance(pair.second.data_begin,
                                      pair.second.data_end);
    }
    lava::info() << "Total entries: " << sPackages.size();
}

std::string string(const std::string &filename) {
    auto package = get(filename);
    return {package.data_begin, package.data_end};
}

SharedShaderModule shader(const SharedDevice &device, const std::string &name) {
    auto package = get(name);
    auto sha = device->createShader(package.data_begin, package.data_end);
    auto stage = identifyShader(name);
    if (stage) {
        sha->setStage(stage.value());
    }
    return sha;
}
}
}
