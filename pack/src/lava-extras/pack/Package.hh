#pragma once

namespace lava {
namespace pack {

struct Package {
    char const* data_begin;
    char const* data_end;
};

}
}
